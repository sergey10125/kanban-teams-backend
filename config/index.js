require('dotenv').config()

exports.app = {
  port: process.env.APP_PORT,
  host: process.env.APP_HOST,
}

exports.auth = {
  resetTokenSize: +process.env.AUTH_RESET_TOKEN_SIZE,
  resetTokenLife: +process.env.AUTH_RESET_TOKEN_LIFETIME,
}

exports.database = {
  user: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME,
  host: process.env.DB_HOSTNAME,
  port: process.env.DB_PORT,
}

exports.session = {
  secret: process.env.SESSION_SECRET,
}

exports.mailService = {
  host: process.env.MAIL_SERVICE_HOST,
  username: process.env.MAIL_SERVICE_USERNAME,
  password: process.env.MAIL_SERVICE_PASSWORD,
  transport: process.env.MAIL_SERVICE_TRANSPORT,
  label: process.env.MAIL_SERVICE_LABEL,
}
