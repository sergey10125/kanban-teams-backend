const { Model } = require('../index');

class ChatRoom extends Model {
  static get tableName() {
    return 'chat_room';
  }

  static get relationMappings() {
    const ChatMessage = require('./ChatMessage');
    const Team = require('./Team');

    return {
      messages: {
        relation: Model.HasManyRelation,
        modelClass: ChatMessage,
        join: {
          from: 'chat_room.id',
          to: 'chat_message.room_id',
        }
      },
      team: {
        relation: Model.BelongsToOneRelation,
        modelClass: Team,
        join: {
          from: 'chat_room.team_id',
          to: 'team.id',
        }
      },
    }
  }

  $beforeInsert(opt, queryContext) {
    this.createdAt = new Date().toISOString();
    return super.$beforeInsert(opt, queryContext);
  }
}

module.exports = ChatRoom;
