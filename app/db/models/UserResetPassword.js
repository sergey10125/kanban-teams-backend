const { Model } = require('../index');
const UserResetPasswordQueryBuilder = require('../queryBuilders/UserResetPasswordQueryBuilder');

class UserResetPassword extends Model {
  static get tableName() {
    return 'user_reset_password';
  }

  static get idColumn() {
    return ['user_id'];
  }

  static get QueryBuilder() {
    return UserResetPasswordQueryBuilder;
  }

  static get relationMappings() {
    const User = require('./User');

    return {
      user: {
        relation: Model.HasOneRelation,
        modelClass: User,
        join: {
          from: 'user_reset_password.user_id',
          to: 'user.id'
        }
      },
    }
  }
}

module.exports = UserResetPassword;
