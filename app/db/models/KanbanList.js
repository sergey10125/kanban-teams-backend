const { Model } = require('../index');
const KanbanListQueryBuilder = require('../queryBuilders/KanbanListQueryBuilder');


class KanbanList extends Model {
  static get tableName() {
    return 'kanban_list';
  }

  static get QueryBuilder() {
    return KanbanListQueryBuilder;
  }

  static get relationMappings() {
    const KanbanCard = require('./KanbanCard');
    return {
      kanbanCards: {
        relation: Model.HasManyRelation,
        modelClass: KanbanCard,
        join: {
          from: 'kanban_list.id',
          to: 'kanban_card.listId',
        }
      },
    }
  }
}

module.exports = KanbanList;
