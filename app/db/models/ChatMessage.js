const { Model } = require('../index');

class ChatMessage extends Model {
  static get tableName() {
    return 'chat_message';
  }

  static get relationMappings() {
    const ChatRoom = require('./ChatRoom');
    const User = require('./User');

    return {
      messages: {
        relation: Model.BelongsToOneRelation,
        modelClass: ChatRoom,
        join: {
          to: 'chat_message.roomId',
          from: 'chat_room.id',
        }
      },
      author: {
        relation: Model.BelongsToOneRelation,
        modelClass: User,
        join: {
          to: 'chat_message.authorId',
          from: 'user.id',
        }
      },
    }
  }

  $beforeInsert(opt, queryContext) {
    this.createdAt = new Date().toISOString();
    return super.$beforeInsert(opt, queryContext);
  }
}

module.exports = ChatMessage;
