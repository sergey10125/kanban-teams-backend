const fs = require('fs')
const path = require('path')

const getModelFiles = dir => fs.readdirSync(dir)
  .filter(file => (file.indexOf('.') !== 0) && (file !== 'index.js'))

const files = getModelFiles(__dirname)
const models = files.reduce((modelsObj, filename) => {
  const model = require(`./${filename}`)
  if (model) modelsObj[filename.split('.').shift()] = model
  return modelsObj
}, {})

module.exports = models
