const { Model } = require('../index');


class KanbanComment extends Model {
  static get tableName() {
    return 'kanban_comment';
  }
}

module.exports = KanbanComment;
