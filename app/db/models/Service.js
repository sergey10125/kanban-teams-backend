const { Model } = require('../index');
const ServiceQueryBuilder = require('../queryBuilders/ServiceQueryBuilder');

class Service extends Model {
  static get tableName() {
    return 'service';
  }

  static get QueryBuilder() {
    return ServiceQueryBuilder;
  }

  static get relationMappings() {
    const Action = require('./Action');
    const TeamRole = require('./TeamRole');

    return {
      actions: {
        relation: Model.HasManyRelation,
        modelClass: Action,
        join: {
          from: 'service.id',
          to: 'action.serviceId'
        }
      },
      roles: {
        relation: Model.HasManyRelation,
        modelClass: TeamRole,
        join: {
          from: 'service.id',
          to: 'role.serviceId'
        }
      },
    }
  }
}

module.exports = Service;
