const { Model } = require('../index');


class KanbanCard extends Model {
  static get tableName() {
    return 'kanban_card';
  }

  static get relationMappings() {
    const KanbanComment = require('./KanbanComment');
    const KanbanList = require('./KanbanList');
    const KanbanDesk = require('./KanbanDesk');
    const User = require('./User');

    return {
      comments: {
        relation: Model.HasManyRelation,
        modelClass: KanbanComment,
        join: {
          from: 'kanban_card.id',
          to: 'kanban_comment.card_id',
        }
      },
      kanbanList: {
        relation: Model.BelongsToOneRelation,
        modelClass: KanbanList,
        join: {
          from: 'kanban_card.list_id',
          to: 'kanban_list.id',
        }
      },
      kanbanDesk: {
        relation: Model.BelongsToOneRelation,
        modelClass: KanbanDesk,
        join: {
          from: 'kanban_card.desk_id',
          to: 'kanban_desk.id',
        }
      },
      author: {
        relation: Model.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: 'kanban_card.ownerId',
          to: 'user.id',
        }
      },
    }
  }

  $beforeInsert(opt, queryContext) {
    this.createdAt = new Date().toISOString();
    return super.$beforeInsert(opt, queryContext);
  }
}

module.exports = KanbanCard;
