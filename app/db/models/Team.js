const { Model } = require('../index');
const TeamQueryBuilder = require('../queryBuilders/TeamQueryBuilder');


class Team extends Model {
  static get tableName() {
    return 'team';
  }

  static get QueryBuilder() {
    return TeamQueryBuilder;
  }

  static get relationMappings() {

    const User = require('./User');
    const TeamRole = require('./TeamRole');
    const ChatRoom = require('./ChatRoom');

    return {
      users: {
        relation: Model.ManyToManyRelation,
        modelClass: User,
        filter: query => query.select('id', 'username', 'email'),
        join: {
          from: 'team.id',
          through: {
            from: 'user_team.team_id',
            to: 'user_team.user_id',
            extra: ['role_id', 'service_id', 'created_at']
          },
          to: 'user.id'
        }
      },
      userRoles: {
        relation: Model.ManyToManyRelation,
        modelClass: TeamRole,
        join: {
          from: "team.id",
          through: {
            from: "user_team.team_id",
            to: "user_team.role_id",
            extra: ['user_id', 'service_id', 'created_at']
          },
          to: "role.id"
        }
      },
      chatRooms: {
        relation: Model.HasManyRelation,
        modelClass: ChatRoom,
        join: {
          from: 'team.id',
          to: 'chat_room.team_id',
        }
      },
    }
  }


  $beforeInsert(opt, queryContext) {
    this.createdAt = new Date().toISOString();
    return super.$beforeInsert(opt, queryContext);
  }
}

module.exports = Team;
