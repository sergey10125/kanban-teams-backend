const { Model } = require('../index');
const TeamRoleQueryBuilder = require('../queryBuilders/TeamRoleQueryBuilder');

class TeamRole extends Model {
  static get tableName() {
    return 'role';
  }

  static get QueryBuilder() {
    return TeamRoleQueryBuilder;
  }

  static get relationMappings() {
    const Action = require('./Action');
    const Service = require('./Service');

    return {
      permissions: {
        relation: Model.ManyToManyRelation,
        modelClass: Action,
        join: {
          from: 'role.id',
          through: {
            from: 'action_permission.roleId',
            to: 'action_permission.actionId',
          },
          to: 'action.id'
        }
      },
      service: {
        relation: Model.BelongsToOneRelation,
        modelClass: Service,
        join: {
          from: 'role.serviceId',
          to: 'service.id'
        }
      }
    }
  }
}

module.exports = TeamRole;
