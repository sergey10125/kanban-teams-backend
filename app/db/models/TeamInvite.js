const { Model } = require('../index');

class TeamInvite extends Model {
  static get tableName() {
    return 'team_invite';
  }

  static get idColumn() {
    return ['email', 'team_id'];
  }

  static get relationMappings() {
    const Team = require('./Team');

    return {
      team: {
        relation: Model.BelongsToOneRelation,
        modelClass: Team,
        join: {
          from: 'team_invite.team_id',
          to: 'team.id'
        }
      }
    }
  }
}

module.exports = TeamInvite;
