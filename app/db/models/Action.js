const { Model } = require('../index');


class Action extends Model {
  static get tableName() {
    return 'action';
  }

  static get relationMappings() {
    const TeamRole = require('./TeamRole');

    return {
      users: {
        relation: Model.ManyToManyRelation,
        modelClass: TeamRole,
        join: {
          from: 'action.id',
          through: {
            from: 'action_permission.actionId',
            to: 'action_permission.roleId',
          },
          to: 'role.id'
        }
      }
    }
  }
}

module.exports = Action;
