const { Model } = require('../index');
const KanbanDeskQueryBuilder = require('../queryBuilders/KanbanDeskQueryBuilder');


class KanbanDesk extends Model {
  static get tableName() {
    return 'kanban_desk';
  }

  static get QueryBuilder() {
    return KanbanDeskQueryBuilder;
  }

  static get relationMappings() {
    const KanbanList = require('./KanbanList');
    const User = require('./User');

    return {
      kanbanLists: {
        relation: Model.HasManyRelation,
        modelClass: KanbanList,
        join: {
          from: 'kanban_desk.id',
          to: 'kanban_list.desk_id'
        }
      },
      owner: {
        relation: Model.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: 'kanban_desk.ownerId',
          to: 'user.id'
        }
      },
    }
  }

  $beforeInsert() {
    this.createdAt = new Date().toISOString();
  }
}

module.exports = KanbanDesk;
