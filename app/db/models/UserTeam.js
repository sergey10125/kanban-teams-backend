const { Model } = require('../index');

class UserTeam extends Model {
  static get tableName() {
    return 'user_team';
  }

  static get idColumn() {
    return ['user_id', 'team_id', 'service_id'];
  }

  static get relationMappings() {
    const Team = require('./Team');
    const User = require('./User');
    const TeamRole = require('./TeamRole');

    return {
      team: {
        relation: Model.BelongsToOneRelation,
        modelClass: Team,
        join: {
          from: 'user_team.teamId',
          to: 'team.id'
        }
      },
      user: {
        relation: Model.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: 'user_team.userId',
          to: 'user.id'
        }
      },
      role: {
        relation: Model.BelongsToOneRelation,
        modelClass: TeamRole,
        join: {
          from: 'user_team.roleId',
          to: 'role.id'
        }
      }
    }
  }
}

module.exports = UserTeam;
