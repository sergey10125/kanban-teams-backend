const { Model } = require('../index');
const UserQueryBuilder = require('../queryBuilders/UserQueryBuilder');

class User extends Model {
  static get tableName() {
    return 'user';
  }

  static get QueryBuilder() {
    return UserQueryBuilder;
  }

  static get relationMappings() {
    const Team = require('./Team');
    const TeamRole = require('./TeamRole');
    // const UserTeam = require('./UserTeam');
    const UserResetPassword = require('./UserResetPassword');

    return {
      teams: {
        relation: Model.ManyToManyRelation,
        modelClass: Team,
        join: {
          from: 'user.id',
          through: {
            from: 'user_team.user_id',
            to: 'user_team.team_id',
            extra: ['role_id, service_id']
          },
          to: 'team.id'
        }
      },
      // bindindgTeamRecords: {
      //   relation: Model.HasManyRelation,
      //   modelClass: userTeam,
      //   join: {
      //     fromto: 'user.id',
      //     to: 'user_team.user_id'
      //   }
      // },
      resetPasswordData: {
        relation: Model.HasOneRelation,
        modelClass: UserResetPassword,
        join: {
          from: 'user.id',
          to: 'user_reset_password.user_id'
        }
      }
    }
  }
  //
  // $beforeInsert(opt, queryContext) {
  //   this.updatedAt = new Date().toISOString();
  //   this.createdAt = new Date().toISOString();
  //
  //   return super.$beforeInsert(opt, queryContext);
  // }
  //
  // $beforeUpdate(queryContext) {
  //   this.updatedAt = new Date().toISOString();
  //
  //   return super.$beforeUpdate(queryContext);
  // }
}

module.exports = User;
