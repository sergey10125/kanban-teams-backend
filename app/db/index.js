const { Model, knexSnakeCaseMappers } = require('objection');
const knexfile = require('../../knexfile')

const env = process.env.NODE_ENV || 'development'

const knex = require('knex')({
  ...knexfile[env],
  ...knexSnakeCaseMappers()
})

Model.knex(knex);

exports.knex = knex
exports.Model = Model
