const { QueryBuilder }  = require('objection');
const bcrypt = require('bcryptjs');

class UserQueryBuilder extends QueryBuilder {
  findByEmail(email) {
    return this.select('id', 'username', 'email').findOne({
      email: email.toLowerCase()
    });
  }
  findWithTeamRole(userId, teamId) {
    return this.select(
        'user.username',
        'user.email',
        'role_id',
        'service.title as serviceTitle'
      )
      .leftJoin('user_team', 'user.id', 'user_team.user_id')
      .innerJoin('service', 'user_team.service_id', 'service.id')
      .where('user.id', userId)
      .andWhere('user_team.team_id', teamId)
      .then((userWithRoles) => {
        if (!userWithRoles) return null

        const teamRoles = userWithRoles.reduce((obj, item) => {
          obj[item.serviceTitle] = item.roleId
          return obj
        }, {})

        return {
          id: +userId,
          username: userWithRoles[0].username,
          email: userWithRoles[0].email,
          teamRoles
        }
      })
  }
  changePass(userId, password) {
    const salt = bcrypt.genSaltSync(10)
    const hashPass = bcrypt.hashSync(password, salt).toString('hex')
    return this.patch({ password: hashPass }).where({ id: userId })
  }

}


module.exports = UserQueryBuilder;
