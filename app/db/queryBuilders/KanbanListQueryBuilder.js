const { QueryBuilder }  = require('objection');

class KanbanListQueryBuilder extends QueryBuilder {
  getByDeskWithCard(deskId) {
    return this.eager('cards')
      .select('id', 'title', 'width')
      .modifyEager('cards', builder => {
        builder.select('id', 'ownerId', 'title', 'createdAt', 'positionY', 'positionX')
        builder.where({ isArchived: false })
        builder.orderBy(['position_y', 'position_x'])
      })
      .where('deskId', deskId)
      .andWhere('is_archived', false)
      .orderBy('position')
  }
}


module.exports = KanbanListQueryBuilder;
