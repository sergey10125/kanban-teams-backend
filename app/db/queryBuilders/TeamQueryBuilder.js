const { QueryBuilder }  = require('objection');

const TEAM_SERVICE_ID = 1;
const TEAM_OWNER_ROLE = {
  service_id: TEAM_SERVICE_ID,
  role_id: 11
};

const MAIN_CHAT_ROOM_TEMPLATE = {
  title: "Global",
  isMain: true
}


class TeamQueryBuilder extends QueryBuilder {
  getByUser(userId) {
    return this.select('id', 'title', 'description', 'team.createdAt')
      .leftJoin('user_team', 'user_team.team_id', 'team.id')
      .where('user_team.userId', userId)
      .andWhere('team.isArchived', false)
      .andWhere('user_team.serviceId', TEAM_SERVICE_ID)
      .orderBy('createdAt', 'desc')
  }

  getWithUserPermissions(id, userId) {
    return this
      .select('id', 'title', 'description', 'created_at')
      .findOne({ id, isArchived: false })
      .eager('userRoles.permissions')
      .modifyEager('userRoles', builder =>
        builder
          .select('role.title as role_title', 'service.title as service_title')
          .where('user_id', userId)
          .innerJoin('service', 'role.serviceId', 'service.id')
      )
      .modifyEager('userRoles.permissions', builder => builder.select('title'))
  }

  getTeamUsers(teamId) {
    return this
      .select(
        'user.id',
        'user.username',
        'user.email',
        'user_team.created_at',
        'role.title as roleTitle'
      )
      .leftJoin('user_team', 'user_team.team_id', 'team.id')
      .innerJoin('user', 'user.id', 'user_team.user_id')
      .innerJoin('role', 'role.id', 'user_team.role_id')
      .where('user_team.team_id', teamId)
      .andWhere('user_team.service_id', TEAM_SERVICE_ID)
      .andWhere('user.isArchived', false)
  }

  createTeam(userId, data) {
    return this.insertGraph(
      {
        ...data,
        users: [{ id: userId, ...TEAM_OWNER_ROLE}],
        chatRooms: [ MAIN_CHAT_ROOM_TEMPLATE ]
      },
      { relate: true }
    ).then((team) => {
      delete team.users;
      return team;
    })
  }
  updateTeam(id, data) {
    return this.where({ id }).update(data);
  }
  archivedTeam(id) {
    return this.where({ id }).update({ isArchived: true });
  }
}


module.exports = TeamQueryBuilder;
