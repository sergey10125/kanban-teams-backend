const { QueryBuilder }  = require('objection');

const oneHour = 3600000; // 3600000 milliseconds is are 1 hour

class UserResetPasswordQueryBuilder extends QueryBuilder {
  findToken(token) {
    return this.select('user_id')
      .where('resetPasswordToken', token )
      .andWhere('resetPasswordExpired', '>', new Date())
      .first();
  }


  createToken(userId, token, lifeTime = oneHour) {
    const expiredDate = new Date(Date.now() + lifeTime);

    return this.insert({
      userId,
      resetPasswordToken: token,
      resetPasswordExpired: expiredDate
    })
  }

  updateToken(userId, token, lifeTime = oneHour) {
    const expiredDate = new Date(Date.now() + lifeTime);

    return this
      .patch({ resetPasswordToken: token, resetPasswordExpired: expiredDate })
      .where({ userId })
  }

  clearToken(userId) {
    return this
      .patch({ resetPasswordToken: null, resetPasswordExpired: new Date() })
      .where({ userId })
  }
}


module.exports = UserResetPasswordQueryBuilder;
