const { QueryBuilder }  = require('objection');
const OWNER_ROLE_ID = 11

class ServiceQueryBuilder extends QueryBuilder {
  serviseWithRoles() {
    return this
      .eager('[roles, roles.permissions]')
      .modifyEager('roles', builder =>
        builder.select('id', 'title').whereNot({
          id: OWNER_ROLE_ID
        })
      )
  }
  getPermissionByRoles(roleIdArray) {
    return this.eager('actions')
      .modifyEager('actions', builder => {
        builder.innerJoin(
          'action_permission',
          'action_permission.actionId',
          'action.id'
        )
        builder.whereIn('action_permission.roleId', roleIdArray)
      })
      .then((values) => values.reduce((obj, service) => {
        obj[service.title] = service.actions.reduce((arr, action) => {
          arr.push(action.title)
          return arr
        }, [])
        return obj
      }, {}))
  }
}


module.exports = ServiceQueryBuilder;
