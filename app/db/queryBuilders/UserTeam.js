const { Model } = require('../index');


class UserTeam extends Model {
  static get tableName() {
    return 'user_team';
  }
  static get idColumn() {
    return ['user_id', 'team_id', 'service_id'];
  }

  $beforeInsert(opt, queryContext) {
    if (!this.createdAt)
      this.createdAt = new Date().toISOString();
    return super.$beforeInsert(opt, queryContext);
  }
}

module.exports = UserTeam;
