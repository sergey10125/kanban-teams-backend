const { QueryBuilder }  = require('objection');

class TeamRoleQueryBuilder extends QueryBuilder {
  getById(ids) {
    const arr = Array.isArray(ids)? ids : [ids]
    return this
      .select('role.id as roleId', 'service.title as serviceTitle')
      .innerJoin('service', 'role.service_id', 'service.id')
      .whereIn('role.id', arr)
      .then((values) => {
        return values.reduce((obj, item) => {
          obj[item.serviceTitle] = item.roleId
          return obj
        }, {})
      })
  }
}


module.exports = TeamRoleQueryBuilder;
