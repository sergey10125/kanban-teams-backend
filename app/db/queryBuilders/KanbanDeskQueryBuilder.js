const { QueryBuilder }  = require('objection');

const DEFAULT_LISTS = [
  { title: 'To-Do', position: 1 },
  { title: 'In processing', position: 2 },
  { title: 'Done', position: 3 }
]

class KanbanDeskQueryBuilder extends QueryBuilder {
  getByTeam(teamId, isArchived = false) {
    return this.where('teamId', teamId)
      .andWhere('is_archived', false)
      .orderBy('createdAt', 'desc')
  }

  create(teamId, title, ownerId, lists = DEFAULT_LISTS) {
    return this.insertGraph({
      title, teamId, ownerId, lists
    });
  }

  updateTitle(id, title) {
    return this.patch({ title }).where({ id });
  }

  archived(id) {
    return this.patch({ isArchived: true }).where({ id });
  }
}


module.exports = KanbanDeskQueryBuilder;
