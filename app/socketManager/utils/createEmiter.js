module.exports = ({ io, socketId = null, userPool }) => {
  const userSocket = io.sockets.connected[socketId];
  const isSocketExist = !!userSocket;

  return {
    sendToUser(userId, data) {
      const userSocketIds = userPool.users[userId];
      if (!userSocketIds) return;

      userSocketIds.forEach(socketId => {
        if (socketId === userSocket.id) return;
        const socket = io.sockets.connected[socketId];
        socket.emit('action', data);
      });
    },

    sendToRoom(room, data) {
      if (isSocketExist) {
        userSocket.to(room).emit('action', data);
      } else {
        io.sockets.in(room).emit('action', data);
      }
    }
  }
}
