const socket = require('socket.io');
const sharedsession = require("express-socket.io-session");

const onJoinRoom = (socket, properties) => ({ token }) => {
  const invite = properties.invites[token];
  if (invite) {
    socket.join(`${invite.roomType}${invite.id}`);
    socket.emit('joinRoomSuccess', invite);
    delete properties.invites[token];
  } else {
    // invalide token notification
  };
};

const onLeaveRoom = (socket, properties) => ({ roomType, id }) => {
  socket.leave(`${roomType}${id}`);
}


const onConnection = (properties) => (socket) => {
  const passport = socket.handshake.session.passport;
  if (!passport || !passport.user) {
    socket.disconnect();
    return;
  };
  const userId = passport.user;
  socket.userId = userId;

  properties.userPool.addUserSocket(userId, socket.id);
  socket.on('disconnect', () => {
    properties.userPool.removeUserSocket(userId, socket.id);
  });

  socket.on('joinRoom', onJoinRoom(socket, properties))
  socket.on('leaveRoom', onLeaveRoom(socket, properties))
}



module.exports = ({ http, session, properties }) => {
  const io = socket(http);

  io.use(sharedsession(session));
  io.on('connection', onConnection(properties));

  return io;
}
