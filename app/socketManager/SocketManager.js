const crypto = require('crypto');

const AppError = require('@app/utils/AppError');
const SocketUserPool = require('./SocketUserPool');

const createSoketServer = require('./utils/createSoketServer');

class SocketManager {
  constructor(props) {
    if (!props.roomTypeArray) {
      throw new Error('require property `roomTypeArray` in SocketManager constructor')
    }

    this._roomTypeArray = props.roomTypeArray;

    this._notificationInitFunc = null
    if (props.notificationInitFunc) {
      this._notificationInitFunc = props.notificationInitFunc;
    }

    this.notification = {};
    this._properties = {
      userPool: new SocketUserPool(),
      roomTreeArr: new Object(),
      invites: new Object(),
    }
  }

  init(http, session) {
    const properties = this._properties;
    const io = createSoketServer({ http, session, properties });

    this.io = io;
    if (this._notificationInitFunc) {
      const notifications = this._notificationInitFunc(io, this._properties);
      Object.keys(notifications).forEach(key => {
        this.notification[key] = notifications[key];
      });
    };
  }

  createInvite(type, data) {
    const roomType = this._roomTypeArray.find(item => item === type);
    if (!roomType) throw new Error('Invalid role type');

    const token = crypto.randomBytes(10).toString('hex');
    data.roomType = roomType;
    this._properties.invites[token] = {
      ...data,
      createAt: new Date()
    };

    return token;
  }
}




module.exports = SocketManager;
