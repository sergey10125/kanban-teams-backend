class SocketUserPool {
  constructor() {
    this.users = {};
  }

  addUserSocket(userId, socketId) {
    if (!userId)
      return;

    if (!this.users[userId])
      this.users[userId] = []

    this.users[userId].push(socketId)
  }

  removeUserSocket(userId, socketId) {
    if (this.users[userId]) {
      this.users[userId] = this.users[userId].filter(
        item => item !== socketId
      );

      if (!this.users[userId].length)
        delete this.users[userId]
    }
  }
}


module.exports = SocketUserPool
