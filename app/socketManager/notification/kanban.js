const createEmiter = require('../utils/createEmiter');

module.exports = (io, properties) => {
  const { userPool } = properties;

  return {
    createKanbanDesk(socketId, teamId, desk) {
      const emiter = createEmiter({ io, userPool, socketId });
      emiter.sendToRoom(`team${teamId}`, {
        action: 'createKanbanDesk',
        data: desk,
        meta: { teamId }
      });
    },
    updateKanbanDeskData(socketId, teamId, desk) {
      const emiter = createEmiter({ io, userPool, socketId });
      emiter.sendToRoom(`team${teamId}`, {
        action: 'updateKanbanDeskData',
        data: desk,
        meta: { deskId: desk.id }
      });
    },
    removeKanbanDesk(socketId, teamId, deskId) {
      const emiter = createEmiter({ io, userPool, socketId });
      emiter.sendToRoom(`team${teamId}`, {
        action: 'removeKanbanDesk',
        meta: { deskId }
      });
    },


    createKanbanList(socketId, deskId, list) {
      const emiter = createEmiter({ io, userPool, socketId });
      emiter.sendToRoom(`kanban${deskId}`, {
        action: 'createKanbanList',
        data: list,
        meta: { deskId }
      });
    },
    updateKanbanListData(socketId, deskId, list) {
      const emiter = createEmiter({ io, userPool, socketId });
      emiter.sendToRoom(`kanban${deskId}`, {
        action: 'updateKanbanListData',
        data: list,
        meta: { deskId, listId: list.id }
      });
    },
    updateKanbanListWidth(socketId, deskId, listId, width) {
      const emiter = createEmiter({ io, userPool, socketId });
      emiter.sendToRoom(`kanban${deskId}`, {
        action: 'updateKanbanListWidth',
        data: { width },
        meta: { deskId, listId  }
      });
    },
    updateKanbanListPosition(socketId, deskId, listId, position) {
      const emiter = createEmiter({ io, userPool, socketId });
      emiter.sendToRoom(`kanban${deskId}`, {
        action: 'updateKanbanListPosition',
        data: { position },
        meta: { deskId, listId }
      });
    },
    removeKanbanList(socketId, deskId, listId) {
      const emiter = createEmiter({ io, userPool, socketId });
      emiter.sendToRoom(`kanban${deskId}`, {
        action: 'removeKanbanList',
        meta: { deskId, listId }
      });
    },


    createKanbanCard(socketId, deskId, listId, card) {
      const emiter = createEmiter({ io, userPool, socketId });
      emiter.sendToRoom(`kanban${deskId}`, {
        action: 'createKanbanCard',
        data: card,
        meta: { deskId, listId }
      });
    },
    updateKanbanCardData(socketId, deskId, card) {
      const emiter = createEmiter({ io, userPool, socketId });
      emiter.sendToRoom(`kanban${deskId}`, {
        action: 'updateKanbanCardData',
        data: card,
        meta: { deskId, cardId: card.id }
      });
    },
    updateKanbanCardPosition(socketId, deskId, cardId, positionData) {
      const emiter = createEmiter({ io, userPool, socketId });
      const { listId, ...position } = positionData;
      emiter.sendToRoom(`kanban${deskId}`, {
        action: 'updateKanbanCardPosition',
        data: { position },
        meta: { deskId, cardId, listId }
      });
    },
    removeKanbanCard(socketId, deskId, cardId) {
      const emiter = createEmiter({ io, userPool, socketId });
      emiter.sendToRoom(`kanban${deskId}`, {
        action: 'removeKanbanCard',
        meta: { deskId, cardId }
      });
    },
  }
}
