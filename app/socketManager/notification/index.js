const fs = require('fs')
const path = require('path')

const getFiles = dir => fs.readdirSync(dir)
  .filter(file => (file.indexOf('.') !== 0) && (file !== 'index.js'))

module.exports = (io, properties) => {
  const files = getFiles(__dirname);

  return files.reduce((obj, filename) => {
    const functionPool = require(`./${filename}`)(io, properties);

    Object.keys(functionPool).forEach(key => {
      if (!!obj[key]) throw new Error(`Duplicate norification function "${key}"`);
      obj[key] = functionPool[key];
    })

    return obj;
  }, new Object())
}
