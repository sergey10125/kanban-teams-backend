const createEmiter = require('../utils/createEmiter');

module.exports = (io, properties) => {
  const { userPool } = properties;
  return {
    editTeamData(socketId, team, memberIds) {
      const emiter = createEmiter({ io, userPool, socketId });
      memberIds.forEach(memberId => {
        emiter.sendToUser(memberId, {
          action: 'editTeamData',
          data: team,
          meta: {
            teamId: team.id
          }
        })
      });
    },
    deleteTeam(socketId, teamId, memberIds) {
      const emiter = createEmiter({ io, userPool, socketId });
      memberIds.forEach(memberId => {
        emiter.sendToUser(memberId, {
          action: 'deleteTeam',
          meta: { teamId }
        })
      });
    },

    createTeamInvite(socketId, teamId, invite) {
      const emiter = createEmiter({ io, userPool, socketId });
      emiter.sendToRoom(`team${teamId}`, {
        action: 'createTeamInvite',
        data: invite,
        meta: { teamId }
      });
    },
    updateTeamInvite(socketId, teamId, invite) {
      const emiter = createEmiter({ io, userPool, socketId });
      emiter.sendToRoom(`team${teamId}`, {
        action: 'updateTeamInvite',
        data: invite,
        meta: { teamId }
      });
    },
    deleteTeamInvite(socketId, teamId, token) {
      const emiter = createEmiter({ io, userPool, socketId });
      emiter.sendToRoom(`team${teamId}`, {
        action: 'deleteTeamInvite',
        data: { token },
        meta: { teamId }
      });
    },

    addTeamMember(teamId, token, member) {
      const emiter = createEmiter({ io, userPool });

      emiter.sendToRoom(`team${teamId}`, {
        action: 'deleteTeamInvite',
        data: { token },
        meta: { teamId }
      });

      emiter.sendToRoom(`team${teamId}`, {
        action: 'addTeamMember',
        data: member,
        meta: { teamId }
      });
    },
    updateMemberRolesInTeam(socketId, teamId, memberId, teamRoles) {
      const emiter = createEmiter({ io, userPool, socketId });

      emiter.sendToUser(memberId, {
        action: 'updateTeamRoles',
        data: teamRoles,
        meta: { teamId }
      });
    },
    updateTeamMember(socketId, teamId, member) {
      const emiter = createEmiter({ io, userPool, socketId });

      emiter.sendToRoom(`team${teamId}`, {
        action: 'updateTeamMember',
        data: member,
        meta: { teamId, memberId: member.id }
      });

    },
    removeTeamMember(socketId, teamId, memberId) {
      const emiter = createEmiter({ io, userPool, socketId });

      emiter.sendToUser(memberId, {
        action: 'deleteTeam',
        meta: { teamId }
      });
      emiter.sendToRoom(`team${teamId}`, {
        action: 'deleteTeamMember',
        meta: { teamId, memberId }
      });
    },
  }
}
