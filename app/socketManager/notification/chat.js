const createEmiter = require('../utils/createEmiter');

module.exports = (io, properties) => {
  const { userPool } = properties;

  return {
    createChatRoom(socketId, teamId, chatRoom) {
      const emiter = createEmiter({ io, userPool, socketId });
      emiter.sendToRoom(`team${teamId}`, {
        action: 'createChatRoom',
        data: chatRoom,
        meta: { teamId }
      });
    },
    updateChatRoom(socketId, teamId, chatRoom) {
      const emiter = createEmiter({ io, userPool, socketId });
      emiter.sendToRoom(`team${teamId}`, {
        action: 'updateChatRoom',
        data: chatRoom,
        meta: { roomId: chatRoom.id }
      });
    },
    removeChatRoom(socketId, teamId, roomId) {
      const emiter = createEmiter({ io, userPool, socketId });
      emiter.sendToRoom(`team${teamId}`, {
        action: 'removeChatRoom',
        meta: { teamId, roomId }
      });
    },

    createChatRoomMessage(socketId, teamId, roomId, message) {
      const emiter = createEmiter({ io, userPool, socketId });
      emiter.sendToRoom(`team${teamId}`, {
        action: 'chatMessageNotification',
        data: message,
        meta: { teamId, roomId }
      });
    },
  }
}
