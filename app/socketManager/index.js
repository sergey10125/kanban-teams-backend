const SocketManager = require('./SocketManager');

const notificationInitFunc = require('./notification');
const roomTypeArray = ['team', 'kanban', 'chat'];

const socketManager = new SocketManager({
  notificationInitFunc,
  roomTypeArray
});

module.exports = socketManager;
