const createExpressApp = require('./utils/createExpressApp');
const socketManager = require('./socketManager');
const api = require('./modules');

const app = createExpressApp()
app.use('/api', api);

const http = require('http').createServer(app);
socketManager.init(http, app.get('session'));

module.exports = http;
