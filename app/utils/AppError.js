class AppError extends Error {

  constructor(code, description  = null) {
      super(code)
      Error.captureStackTrace(this, AppError);

      this.code = code;
      this.isOperational  = true;

      if (description) {
        let errDescription = description;
        if (typeof description !== 'object') {
          errDescription = { _error: description };
        };

        this.description  = errDescription;
      };

  }
}

module.exports = AppError
