const { validationResult } = require('express-validator');
const AppError = require('@app/utils/AppError');

const errorFormatter = ({ param: key, msg: value }) => ({ key, value });

module.exports = (req, res, next) => {
  const result = validationResult(req).formatWith(errorFormatter);

  if (!result.isEmpty()) {
    const errorData = result.array().reduce((obj, { key, value }) => {
      obj[key] = value;
      return obj;
    }, new Object());
    
    return next(new AppError(400, errorData));
  };

  next();
}
