const passport = require('passport');
const { Strategy: LocalStrategy } = require('passport-local');
const bcrypt = require('bcryptjs');

const { User } = require('@app/db/models');

passport.use(new LocalStrategy(
  { usernameField: 'email'},
  (email, password, done) =>
    User.query().findOne({ email: email.toLowerCase() }).then((user) => {
      if (user && bcrypt.compareSync(password, user.password)) {
        done(null, {
          id: user.id,
          email: user.email,
          username: user.username,
        });
      } else {
        done(null, false);
      }
    }).catch((err) => done(err))
))


passport.serializeUser((user, done) => done(null, user.id))

passport.deserializeUser((id, done) =>
  User.query().findById(id)
  .then((user) => done(null, {
    id: user.id,
    email: user.email,
    username: user.username,
  }))
  .catch((err) => done(err))
)

module.exports = passport;
