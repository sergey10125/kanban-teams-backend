const express = require('express');
const bodyParser = require('body-parser');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const expressSession = require('express-session');

const { session: sessionConfig } = require('@config');
const passport = require('./passport');

// const bindSocketController = require('./bindSocketController');

module.exports = () => {
  const app = express();

  app.use(logger('tiny'))

  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(bodyParser.json());
  app.use(cookieParser());

  const session = expressSession({
    secret: sessionConfig.secret,
    resave: true,
    saveUninitialized: true,
    cookie: {
      httpOnly: false,
      secure: false,
    },
  });

  app.set('session', session);
  app.use(session)

  app.use(passport.initialize());
  app.use(passport.session());

  app.use((req, res, next) => {
    req.socketId = req.header('X-Socket-Id');
    next();
  });

  return app;
}
