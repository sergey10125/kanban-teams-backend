module.exports = (err, req, res, next) => {
  if (err.isOperational) {
    if (err.description) {
      return res.status(err.code).send(err.description)
    } else {
      return res.sendStatus(err.code)
    }
  } else {
    next(err)
  }
}
