const request = require('request');
const { mailService: config } = require('@config');


module.exports = (to, subject, html) => new Promise((reslove, reject) => {
  request({
    method: 'POST',
    uri: config.host,
    headers: { "content-type": "application/json" },
    auth: {
      user: config.username,
      pass: config.password,
      sendImmediately: false
    },
    body: JSON.stringify({
      to,
      subject,
      html,
      transport: config.transport,
      label: config.label
    })
  }, (error, response, body) => {
    if (!error && response.statusCode == 200) {
      reslove()
    } else {
      reject(error || response.statusCode)
    }
  })
})
