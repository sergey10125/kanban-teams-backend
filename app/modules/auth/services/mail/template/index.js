const path = require('path');
const jade = require('jade');

const { app: { host }} = require('@config');

const templatePath = {
  passwordChange: path.join(__dirname, 'passwordChange.jade'),
  resetPassToken: path.join(__dirname, 'resetPassToken.jade')
}

exports.passwordChange = () => {
  return jade.renderFile(templatePath.passwordChange);
}

exports.resetPassToken = (token) => {
  return jade.renderFile(templatePath.resetPassToken, { host, token });
}
