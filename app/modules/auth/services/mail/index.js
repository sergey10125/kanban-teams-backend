const sendEmail = require('@app/utils/sendEmail');
const template = require('./template');

exports.sendResetPassToken = (email, token) => {
  const context = template.resetPassToken(token);
  return sendEmail(email, 'Reset password', context);
}

exports.sendPassChangeNotification = (email, token) => {
  const context = template.passwordChange();
  return sendEmail(email, 'Reset password', context);
}
