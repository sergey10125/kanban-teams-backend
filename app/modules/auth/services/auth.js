const { transaction } = require('objection');
const bcrypt = require('bcryptjs');
const crypto = require('crypto');

const { auth: { resetTokenLife, resetTokenSize } } = require('@config');
const { User, UserResetPassword: UserRP } = require('@app/db/models');
const { knex } = require('@app/db');
const passport = require('@app/utils/passport');
const AppError = require('@app/utils/AppError');

exports.findByEmail = (email) => {
  return User.query().select('id', 'username', 'email').findOne({
    email: email.toLowerCase()
  });
}

exports.findByResetToken = (token) => {
  return UserRP.query()
    .select('user.id', 'user.username', 'user.email')
    .joinRelation('user')
    .where('resetPasswordToken', token)
    .andWhere('resetPasswordExpired', '>', new Date())
    .first();
}

exports.createUser = (data) => {
  const { email, phone, username, password } = data;

  const salt = bcrypt.genSaltSync(10);
  const hashPass = bcrypt.hashSync(password, salt).toString('hex');

  return User.query().insert({
    email: email.toLowerCase(),
    username,
    phone,
    password: hashPass
  });
}


// RESET TOKEN SERVICE

exports.createResetToken = async (userId) => {
  const userResetPassword = await UserRP.query().findOne('user_id', userId);
  const token = crypto.randomBytes(resetTokenSize).toString('hex');

  if (userResetPassword) {
    await UserRP.query().updateToken(userId, token, resetTokenLife);
  } else {
    await UserRP.query().createToken(userId, token, resetTokenLife);
  };

  return token;
}

exports.resetPassword = async (userId, password) => {
  return knex.transaction((trx) => {
    return Promise.all([
      User.query(trx).changePass(userId, password),
      UserRP.query(trx).clearToken(userId)
    ]);
  })
}
