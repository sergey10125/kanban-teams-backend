const AppError = require('@app/utils/AppError');
const passport = require('@app/utils/passport');

const { authService, mailService } = require('./services');

// authentication controllers

exports.getUserData = (req, res, next) => {
  return res.send(req.user)
}

exports.signUp = async (req, res, next) => {
  const userDTO = req.body;

  const user = await authService.createUser(userDTO);

  req.logIn(user, (err) => {
    if (err) return next(err)
    const { id, email, username } = user;
    return res.status(200).send({ id, email, username })
  })
}

exports.signIn = (req, res, next) => {
  return passport.authenticate('local', (err, user, info) => {
    if (err) return next(err)
    if (!user) return next(new AppError(400, 'authFail'))

    req.logIn(user, (err) => {
      if (err) return next(err)
      const { id, email, username } = user;
      return res.status(200).send({ id, email, username })
    })
  })(req, res, next)
}

exports.logout = (req, res, next) => {
  req.logout()
  return res.sendStatus(200)
}


// reset password action controllers

exports.sendResetToken = async (req, res, next) => {
  const { searchUser } = req;

  const token = await authService.createResetToken(searchUser.id);
  await mailService.sendResetPassToken(searchUser.email, token);

  return res.sendStatus(200);
}

exports.resetChangePass = async (req, res, next) => {
  const { resetTokenUser, body: { password } } = req;

  await authService.resetPassword(resetTokenUser.id, password);
  mailService.sendPassChangeNotification(resetTokenUser.email);

  req.logIn(resetTokenUser, (err) => {
    if (err) return next(err)
    const { id, email, username } = resetTokenUser;
    return res.status(200).send({ id, email, username })
  })
}
