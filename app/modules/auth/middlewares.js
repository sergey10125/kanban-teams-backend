const AppError = require('@app/utils/AppError');
const { authService } = require('./services');

exports.checkAuthStatus = (isAuth = true) => (req, res, next) => {
  return (req.isAuthenticated() === isAuth) ? next() : next(new AppError(403));
};

exports.checkEmail = (isMustBeFound = false) => async (req, res, next) => {
  const { body: { email } } = req;

  const user = await authService.findByEmail(email);

  if (isMustBeFound) {
    if (!user) return next(new AppError(400, 'emailNotFound'));
    req.searchUser = user;
  } else if (!!user) {
    return next(new AppError(400, 'emailExist'));
  }

  next();
};

exports.checkToken = async (req, res, next) => {
  const { body: { token } } = req;

  const user = await authService.findByResetToken(token);
  if (!user) return next(new AppError(404));

  req.resetTokenUser = user;
  next();
};
