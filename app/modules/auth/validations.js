const { body } = require('express-validator');
const checkValidation = require('@app/utils/checkValidation');

exports.signUp = [
  body('email').exists().isEmail(),
  body('username').exists(),
  body('password').exists(),
  checkValidation
];

exports.signIn = [
  body('email').exists().isEmail(),
  body('password').exists(),
  checkValidation
];

exports.resetRequest = [
  body('email').exists().isEmail(),
  checkValidation
];


exports.resetTokenVerification = [
  body('token'),
  checkValidation
];

exports.resetChangePass = [
  body('token').exists(),
  body('password').exists(),
  checkValidation
];
