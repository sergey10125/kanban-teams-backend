const express = require('express');

const authController = require('./controllers');
const validations = require('./validations');
const {
  checkAuthStatus,
  checkEmail,
  checkToken
} = require('./middlewares');

const router = express.Router();

// default auth password route

router.get(
  '/getUserData',
  checkAuthStatus(true),
  authController.getUserData
);
router.post(
  '/signUp',
  checkAuthStatus(false),
  validations.signUp,
  checkEmail(),
  authController.signUp
);
router.post(
  '/signIn',
  checkAuthStatus(false),
  validations.signIn,
  authController.signIn
);
router.post(
  '/logout',
  checkAuthStatus(true),
  authController.logout
);

// reset password route

router.post(
  '/resetRequest',
  checkAuthStatus(false),
  validations.resetRequest,
  checkEmail(true),
  authController.sendResetToken
);
router.post(
  '/resetTokenVerification',
  checkAuthStatus(false),
  validations.resetTokenVerification,
  checkToken,
  (req, res) => res.sendStatus(200)
);
router.post(
  '/resetChangePass',
  checkAuthStatus(false),
  validations.resetChangePass,
  checkToken,
  authController.resetChangePass
);



module.exports = router;
