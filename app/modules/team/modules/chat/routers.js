const express = require('express');

const { hasPermission } = require('../../middlewares');

const controllers = require('./controllers');
const validations = require('./validations');
const middlewares = require('./middlewares');

const router = express.Router();

router.get('/', controllers.getChatRoomListByTeam);

router.post(
  '/',
  hasPermission('createChatRoom'),
  validations.chatRoomData,
  controllers.createChatRoom
);

router.get(
  '/:roomId',
  middlewares.checkChatRoom,
  controllers.getChatRoom
);

router.put(
  '/:roomId',
  hasPermission('updateChatRoom'),
  middlewares.checkChatRoom,
  validations.chatRoomData,
  controllers.updateChatRoom
);

router.delete(
  '/:roomId/notification',
  hasPermission('removeChatRoom'),
  middlewares.checkChatRoom,
  controllers.clearChatRoomNotification
);

router.delete(
  '/:roomId',
  middlewares.checkChatRoom,
  controllers.removeChatRoom
);

router.get(
  '/:roomId/message',
  middlewares.checkChatRoom,
  controllers.getMessageList
);
router.post(
  '/:roomId/message',
  hasPermission('createChatMessage'),
  middlewares.checkChatRoom,
  validations.chatMessageData,
  controllers.createMessage
);

module.exports = router;
