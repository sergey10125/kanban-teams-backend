const { body } = require('express-validator');
const checkValidation = require('@app/utils/checkValidation');

exports.chatRoomData = [
  body('title').exists().isString().isLength({ max: 255 }).not().isEmpty(),
  checkValidation
];

exports.chatMessageData = [
  body('content').exists().isString().not().isEmpty(),
  checkValidation
];
