const socketManager = require('@app/socketManager');
const AppError = require('@app/utils/AppError');

const services = require('./services');

const { notification: socketNotification } = socketManager;

exports.getChatRoomListByTeam = async (req, res, next) => {
  const { user, team } = req;
  const roomList = await services.getChatRoomListByTeam(team.id, user.id);
  return res.status(200).send(roomList);
}

exports.getChatRoom = (req, res, next) => {
  const { chatRoom } = req;
  const invite = socketManager.createInvite('chat', { id: chatRoom.id });
  return res.status(200).send({
    chat: chatRoom,
    invite
  });
}

exports.createChatRoom = async (req, res, next) => {
  const socketId = req.header('X-Socket-Id');
  const { team, body: chatRoomDTO } = req;

  const chatRoom = await services.createChatRoom(team.id, chatRoomDTO);

  socketNotification.createChatRoom(socketId, team.id, chatRoom);
  return res.status(200).send(chatRoom);
}

exports.updateChatRoom = async (req, res, next) => {
  const socketId = req.header('X-Socket-Id');
  const { team, chatRoom, body: chatRoomDTO } = req;
  const updatedChatRoom = await services.updateChatRoom(
    chatRoom.id,
    chatRoomDTO
  );;

  socketNotification.updateChatRoom(socketId, team.id, updatedChatRoom);
  return res.status(200).send(updatedChatRoom);
}

exports.clearChatRoomNotification = async (req, res, next) => {
  const { user, chatRoom } = req;
  await services.removeChatRoomNotificationForMember(chatRoom.id, user.id);
  return res.sendStatus(200);
}

exports.removeChatRoom = async (req, res, next) => {
  const socketId = req.header('X-Socket-Id');
  const { team, chatRoom: { id: roomId } } = req;

  await services.removeChatRoom(roomId);

  socketNotification.removeChatRoom(socketId, team.id, roomId);
  return res.sendStatus(200);
}


exports.getMessageList = async (req, res, next) => {
  const socketId = req.header('X-Socket-Id');
  const { chatRoom, query: { lastDatetime } } = req;
  const messageList = await services.getMessageListByChatRoom(
    chatRoom.id,
    lastDatetime
  );

  return res.status(200).send(messageList);
}

exports.createMessage = async (req, res, next) => {
  const socketId = req.header('X-Socket-Id');
  const { user, team, chatRoom, body: { content } } = req;
  const message = await services.createMessage(user.id, chatRoom, content);

  delete message.authorId;
  delete message.roomId;
  message.author = {
    id: user.id,
    username: user.username
  };

  socketNotification.createChatRoomMessage(
    socketId, team.id, chatRoom.id, message
  );

  return res.status(200).send(message);
}
