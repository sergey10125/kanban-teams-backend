const AppError = require('@app/utils/AppError');
const services = require('./services');


exports.checkChatRoom = async (req, res, next) => {
  const { team, params: { roomId } } = req;

  const chatRoom = await services.getChatRoom(roomId);
  if (!chatRoom) return next(new AppError(404));
  if (chatRoom.teamId !== team.id) return next(new AppError(403));

  req.chatRoom = chatRoom;
  return next();
}
