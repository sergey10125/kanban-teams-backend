const { transaction } = require('objection');

const { ChatRoom, ChatMessage } = require('@app/db/models');
const { knex } = require('@app/db');
const AppError = require('@app/utils/AppError');

// Chat room action

exports.getChatRoomListByTeam = (teamId, memberId) => {
  return ChatRoom.query()
    .select(
      'chat_room.id',
      'chat_room.is_main',
      'chat_room.title',
      'chat_room.created_at',
      'chat_room.created_at',
      knex.raw('count(notification.id)::INTEGER as "notificationCount"')
    )
    .leftJoin(
      knex('chat_message').select('id', 'room_id')
        .innerJoin('message_notification', 'chat_message.id', 'message_notification.message_id')
        .where('message_notification.user_id', memberId)
        .as('notification'),
      function() {
        this.on('chat_room.id', '=', 'notification.room_id');
      }
    )
    .where('chat_room.team_id', teamId)
    .groupBy('chat_room.id')
    .orderBy([
      { column: 'chat_room.isMain', order: 'desc' },
      { column: 'chat_room.createdAt', order: 'abs' }
    ])
}

exports.getChatRoom = (roomId) => {
  return ChatRoom.query().findOne({ id: roomId });
}

exports.createChatRoom = (teamId, chatRoomDTO) => {
  const { title } = chatRoomDTO;

  return ChatRoom.query()
    .insert({
      teamId,
      title,
      isMain: false
    })
    // HACK: rewrite this shite later
    .then((value) => {
      delete value.teamId;
      return value;
    })
}

exports.updateChatRoom = (roomId, chatRoomDTO) => {
  const { title } = chatRoomDTO;

  return ChatRoom.query()
    .returning(['id', 'isMain', 'title', 'createdAt'])
    .update({ title })
    .where({ id: roomId })
    .first()
}


exports.removeChatRoomNotificationForMember = (roomId, memberId) => {
  return knex('message_notification')
    .whereIn('message_notification.message_id', function() {
      this.select('chat_message.id').from('chat_message')
        .innerJoin('message_notification', 'chat_message.id', 'message_notification.message_id')
        .innerJoin('chat_room', 'chat_message.room_id', 'chat_room.id')
        .where('chat_room.id', roomId)
        .andWhere('message_notification.user_id', memberId)
    })
    .del()
}

exports.removeChatRoom = (roomId) => {
  return ChatRoom.query()
    .where({ id: roomId })
    .del();
}


// Chat room message

const MESSAGE_LIST_LIMIT_SIZE = 20;


exports.getMessageListByChatRoom = (roomId, lastDatetime = null) => {
  return ChatMessage.query()
    .select(
      'id',
      'content',
      'content',
      'created_at',
    )
    .eager('author')
    .modifyEager('author', qb => qb.select('id', 'username'))
    .modify((qb) => {
      qb.where({ roomId })
      const date = new Date(lastDatetime);
      if (!!lastDatetime && date instanceof Date && !isNaN(date)) {
        qb.andWhere('createdAt', '<', lastDatetime)
      }
    })
    .orderBy('createdAt', 'DESC')
    .limit(MESSAGE_LIST_LIMIT_SIZE)
    .then((value) => value.reverse())
}

exports.createMessage = (userId, room, content) => {
  const { id: roomId, teamId } = room;

  return knex.transaction( async (trx) => {
    const message = await ChatMessage.query(trx).insert({
      authorId: userId,
      roomId,
      content
    });

    await knex
      .transacting(trx)
      .from(knex.raw('message_notification (message_id, user_id)'))
      .insert(function() {
        this.from('user_team as U_T')
          .whereNot('U_T.user_id', userId)
          .andWhere({
            serviceId: 1,
            teamId: teamId
          })
          .select(
            knex.raw('? AS ??', [message.id, 'message_id']),
            'U_T.user_id as user_id',
          )
      });

    return message;
  })

}
