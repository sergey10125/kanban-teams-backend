const fs = require('fs')
const path = require('path')

const getFiles = dir => fs.readdirSync(dir)
  .filter(file => (file.indexOf('.') !== 0) && (file !== 'index.js'))

const files = getFiles(__dirname)
const modules = files.reduce((obj, filename) => {
  const module = require(`./${filename}`);
  if (module) obj[`${filename.split('.').shift()}Module`] = module;
  return obj;
}, new Object())

module.exports = modules
