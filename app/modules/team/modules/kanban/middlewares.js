const AppError = require('@app/utils/AppError');
const services = require('./services');

exports.checkKanbanDesk = async (req, res, next) => {
  const { team, params: { deskId } } = req;

  const kanbanDesk = await services.getKanbanDesk(deskId);

  if (!kanbanDesk) return next(new AppError(404));
  if (kanbanDesk.teamId === team.id) return next(new AppError(403));

  req.kanbanDesk = kanbanDesk;
  return next();
}

exports.checkKanbanList = async (req, res, next) => {
  const { kanbanDesk, params: { listId } } = req;

  const kanbanList = await services.getList(listId);
  if (!kanbanList) return next(new AppError(404));
  if (kanbanList.deskId !== kanbanDesk.id) return next(new AppError(403));

  req.kanbanList = kanbanList;
  return next();
}

exports.checkKanbanCard = async (req, res, next) => {
  const { kanbanDesk, params: { cardId } } = req;

  const kanbanCard = await services.getCard(cardId);

  if (!kanbanCard) return next(new AppError(404));
  if (kanbanCard.deskId !== kanbanDesk.id) return next(new AppError(403));

  req.kanbanCard = kanbanCard;
  return next();
}
