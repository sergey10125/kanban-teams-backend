exports.changeListWidth = (cardArr, width) => {
  let changedCardArr = []
  let rowArr = []
  let tmpArr = []

  for(let i=0; i <= cardArr.length - 1; i++) {
    const card = cardArr[i]
    const nextCard = cardArr[i + 1]
    const prevCard = rowArr[rowArr.length - 1]

    if (!!prevCard) {
      const positionDifference = card.positionX - prevCard.positionX

      if (positionDifference > 1)
        rowArr = rowArr.concat(new Array(positionDifference - 1))

    } else if (card.positionX !== 1) {
      const emptyArr = new Array(card.positionX - 1)
      rowArr = emptyArr.concat(rowArr)
    }


    rowArr.push(card)
    if (!nextCard || card.positionY !== nextCard.positionY) {
      const positionY = card.positionY
      rowArr = tmpArr.concat(rowArr)
      tmpArr = []

      // clear all extra array empty item
      for (let i = rowArr.length - 1; i >= 0; i--) {
        if (rowArr.length <= width) break;
        if (!rowArr[i]) rowArr.splice(i, 1)
      }

      // cut off the excess cards and save them in tmpArr
      if (rowArr.length > width) {
        tmpArr = rowArr.splice(width)
      }

      //save card in new array with new positions
      for (let x = 0; x < rowArr.length; x++) {
        const item = rowArr[x]
        if (!!item) {
          item.positionY = positionY
          item.positionX = x + 1
          changedCardArr.push(item)
        }
      }
      rowArr = []
      const positionYDiff = !!nextCard && nextCard.positionY - positionY
      if (!!tmpArr.length && !!positionYDiff && positionYDiff > 1) {
        for (let y=1; y < positionYDiff; y++) {
          for (let x=1; x <= width; x++) {
            let item = tmpArr.shift()
            item.positionY = positionY + y
            item.positionX = x
            changedCardArr.push(item)

            if (!tmpArr.length)
              break
          }
          if (!tmpArr.length)
            break
        }
      }
    }
  }

  if (!!tmpArr.length) {
    let positionY = changedCardArr[changedCardArr.length - 1].positionY + 1
    let positionX = 1
    while(tmpArr.length) {
      const item = tmpArr.shift()
      item.positionY = positionY
      item.positionX = positionX
      positionX += 1
      changedCardArr.push(item)

      if (positionX > width) {
        positionY += 1
        positionX = 1
      }
    }
  }

  return changedCardArr
};


exports.insertCardToList = (listCards, listWidth, card) => {
  let insertCard = card;
  let cards = listCards.filter(item => item.id !== card.id)

  if (cards.length) {
    for (let i=0; i < cards.length; i++) {
      const card = cards[i]
      if (
        card.positionX == insertCard.positionX &&
        card.positionY == insertCard.positionY
      ) {
        const tmp = card
        cards[i] = insertCard
        insertCard = tmp

        if (insertCard.positionX == listWidth) {
          insertCard.positionX = 1;
          insertCard.positionY += 1;
        } else {
          insertCard.positionX += 1;
        }

      } else if (
        card.positionX > insertCard.positionX &&
        card.positionY > insertCard.positionY
      ) {
        cards.splice(i, 0, insertCard);
        insertCard = null;
        break;
      }
    }

    if (insertCard){
      cards.push(insertCard)
    }

  } else {
    cards.push(insertCard)
  }

  return cards
}
