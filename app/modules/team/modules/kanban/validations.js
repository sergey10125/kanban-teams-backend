const { body } = require('express-validator');
const checkValidation = require('@app/utils/checkValidation');

exports.kanbanDeskData = [
  body('title').exists().isString().isLength({ max: 255 }).not().isEmpty(),
  checkValidation
];


exports.listData = [
  body('title').exists().isString().isLength({ max: 255 }).not().isEmpty(),
  checkValidation
];

exports.updateKanbanListWidth = [
  body('width').exists().isInt({ min: 1, max: 5 }),
  checkValidation
];

exports.updateListPosition = [
  body('position').exists().isInt({ min: 0 }),
  checkValidation
];


exports.createCardData = [
  body('title').exists().isString().isLength({ max: 255 }).not().isEmpty(),
  body('position.x').exists().isInt(),
  body('position.y').exists().isInt(),
  checkValidation
];

exports.updateCardData = [
  body('title').exists().isString().isLength({ max: 255 }).not().isEmpty(),
  checkValidation
];


exports.updateCardPosition = [
  body('position.x').exists().isInt(),
  body('position.y').exists().isInt(),
  checkValidation
];
