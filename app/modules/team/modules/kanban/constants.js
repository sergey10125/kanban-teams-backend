exports.initialKanbanDeskList = [
  { title: 'To-Do', position: 1 },
  { title: 'In processing', position: 2 },
  { title: 'Done', position: 3 }
];

exports.initialKanbanListWidth = 2;
