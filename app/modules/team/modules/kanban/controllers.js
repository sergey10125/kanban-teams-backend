const socketManager = require('@app/socketManager');
const AppError = require('@app/utils/AppError');

const services = require('./services');

const { notification: socketNotification } = socketManager;

exports.getKanbanDeskListByTeam = async (req, res, next) => {
  const { team } = req;
  const kanbanDeskList = await services.getKanbanDeskListByTeam(team.id);
  return res.status(200).send(kanbanDeskList);
}

exports.getKanbanDeskWithList = async (req, res, next) => {
  const { kanbanDesk } = req;

  kanbanDesk.lists = await services.getListWithCard(kanbanDesk.id);
  const invite = socketManager.createInvite('kanban', { id: kanbanDesk.id });

  return res.status(200).send({
    desk: kanbanDesk,
    invite
  });
}

exports.createKanbanDesk = async (req, res, next) => {
  const socketId = req.header('X-Socket-Id');
  const { user, team, body: deskDTO } = req;

  const kanbanDesk = await services.createKanbanDesk(
    team.id, user.id, deskDTO
  );

  kanbanDesk.owner = { id: user.id, username: user.username };
  delete kanbanDesk.kanbanLists;
  delete kanbanDesk.ownerId;
  delete kanbanDesk.teamId;

  socketNotification.createKanbanDesk(socketId, team.id, kanbanDesk)
  return res.status(200).send(kanbanDesk);
}

exports.updateKanbanDesk = async (req, res, next) => {
  const socketId = req.header('X-Socket-Id');
  const { team, kanbanDesk, body: deskDTO } = req;
  const { id: deskId } = kanbanDesk;

  const updateKanbanDesk = await services.updateKanbanDesk(deskId, deskDTO);

  updateKanbanDesk.owner = kanbanDesk.owner;
  delete updateKanbanDesk.isArchived;
  delete updateKanbanDesk.ownerId;
  delete updateKanbanDesk.teamId;

  socketNotification.updateKanbanDeskData(socketId, team.id, updateKanbanDesk)
  return res.status(200).send(updateKanbanDesk);
}

exports.removeKanbanDesk = async (req, res, next) => {
  const socketId = req.header('X-Socket-Id');
  const { team, kanbanDesk: { id: deskId } } = req;

  await services.removeKanbanDesk(deskId);

  socketNotification.removeKanbanDesk(socketId, team.id, deskId);
  return res.sendStatus(200);
}

exports.createKanbanList = async (req, res, next) => {
  const socketId = req.header('X-Socket-Id');
  const { kanbanDesk, body: listDTO } = req;
  const { id: deskId } = kanbanDesk;

  const kanbanList = await services.createList(deskId, listDTO);

  delete kanbanList.deskId;
  delete kanbanList.position;

  socketNotification.createKanbanList(socketId, deskId, kanbanList);
  return res.status(200).send(kanbanList);
}

exports.updateKanbanListData = async (req, res, next) => {
  const socketId = req.header('X-Socket-Id');
  const { kanbanDesk, kanbanList, body: listDTO } = req;
  const { id: deskId } = kanbanDesk;

  const updateKanbanList = await services.updateListData(
    kanbanList.id, listDTO
  );

  delete updateKanbanList.deskId;
  delete updateKanbanList.position;
  delete updateKanbanList.isArchived;

  socketNotification.updateKanbanListData(socketId, deskId, updateKanbanList);
  return res.status(200).send(updateKanbanList);
}

exports.updateKanbanListWidth = async (req, res, next) => {
  const socketId = req.header('X-Socket-Id');
  const { kanbanDesk, kanbanList, body: { width } } = req;
  const { id: deskId } = kanbanDesk;

  await services.updateListWidth(kanbanList, width);

  socketNotification.updateKanbanListWidth(
    socketId, deskId, kanbanList.id, width
  );

  return res.sendStatus(200);
}

exports.updateKanbanListPosition = async (req, res, next) => {
  const socketId = req.header('X-Socket-Id');
  const { kanbanDesk: { id: deskId }, kanbanList, body: { position } } = req;

  await services.updateListPosition(deskId, kanbanList, position);

  socketNotification.updateKanbanListPosition(
    socketId, deskId, kanbanList.id, position
  );

  return res.sendStatus(200);
}

exports.removeKanbanList = async (req, res, next) => {
  const socketId = req.header('X-Socket-Id');
  const { kanbanDesk: { id: deskId }, kanbanList } = req;

  await services.deleteList(deskId, kanbanList.id);

  socketNotification.removeKanbanList(socketId, deskId, kanbanList.id);
  return res.sendStatus(200);
}

exports.getKanbanCard = (req, res, next) => {
  const { kanbanCard } = req;
  return res.status(200).send({
    id: kanbanCard.id,
    title: kanbanCard.title,
    author: kanbanCard.author,
    listId: kanbanCard.listId,
    description: kanbanCard.description,
    createdAt: kanbanCard.createdAt,
  });
}

exports.createKanbanCard = async (req, res, next) => {
  const socketId = req.header('X-Socket-Id');
  const { user, kanbanDesk, kanbanList, body: cardDTO } = req;
  const { id: deskId } = kanbanDesk;

  const kanbanCard = await services.createCard(user, kanbanList, cardDTO);

  const payload = {
    ...kanbanCard,
    positionX: cardDTO.position.x,
    positionY: cardDTO.position.y
  };

  socketNotification.createKanbanCard(
    socketId, deskId, kanbanList.id, payload
  );

  return res.status(200).send(kanbanCard);
}

exports.updateKanbanCardData = async (req, res, next) => {
  const socketId = req.header('X-Socket-Id');
  const { kanbanDesk, kanbanCard, body: cardDTO } = req;
  const { id: deskId } = kanbanDesk;

  const updateKanbanCard = await services.updateCardData(kanbanCard, cardDTO);

  socketNotification.updateKanbanCardData(socketId, deskId, updateKanbanCard);
  return res.status(200).send(updateKanbanCard);
}

exports.updateKanbanCardPosition = async (req, res, next) => {
  const socketId = req.header('X-Socket-Id');
  const { kanbanDesk, kanbanList, kanbanCard, body: { position } } = req;
  const { id: deskId } = kanbanDesk;

  if (kanbanList.width < position.x) {
    next(new AppError(400, 'InvalideXPosition'))
  }

  await services.updateCardPosition(kanbanCard, kanbanList, position);

  const payload = {
    ...position,
    listId: kanbanList.id
  };
  socketNotification.updateKanbanCardPosition(
    socketId, deskId, kanbanCard.id, payload
  );

  return res.sendStatus(200);
}

exports.removeKanbanCard = async (req, res, next) => {
  const socketId = req.header('X-Socket-Id');
  const { kanbanDesk, kanbanCard: { id: cardId } }= req;
  const { id: deskId } = kanbanDesk;

  await services.removeCard(cardId);

  socketNotification.removeKanbanCard(socketId, deskId, cardId);
  return res.sendStatus(200);
}
