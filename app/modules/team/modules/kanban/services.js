const { transaction } = require('objection');

const AppError = require('@app/utils/AppError');
const { KanbanDesk, KanbanList, KanbanCard } = require('@app/db/models');
const { knex } = require('@app/db');

const constants = require('./constants');
const utils = require('./utils');


exports.getKanbanDeskListByTeam = (teamId) => {
  return KanbanDesk.query()
    .select('id', 'title', 'createdAt')
    .eager('owner')
    .modifyEager('owner', qb => qb.select('id', 'username'))
    .where('teamId', teamId)
    .andWhere('is_archived', false)
    .orderBy('createdAt', 'desc')
};

exports.getKanbanDesk = (deskId) => {
  return KanbanDesk.query()
    .select('id', 'ownerId', 'title', 'createdAt')
    .eager('owner')
    .modifyEager('owner', qb => qb.select('id', 'username'))
    .findOne({
      id: deskId,
      isArchived: false,
    });
};

exports.createKanbanDesk = (teamId, ownerId, deskDTO) => {
  const { title } = deskDTO;

  return KanbanDesk.query()
    .insertGraph({
      title,
      teamId,
      ownerId,
      kanbanLists: constants.initialKanbanDeskList
    });
};

exports.updateKanbanDesk = (deskId, deskDTO) => {
  const { title } = deskDTO;

  return KanbanDesk.query()
    .patch({ title })
    .where({ id: deskId })
    .first()
    .returning('*');;
};


exports.removeKanbanDesk = (deskId) => {
  return KanbanDesk.query()
    .where({ id: deskId })
    .del();
};

exports.getList = (listId) => {
  return KanbanList.query().findOne({
    id: listId,
    isArchived: false,
  });
};

exports.getListWithCard = (deskId) => {
  return KanbanList.query()
    .eager('kanbanCards as cards')
    .select('id', 'title', 'width', 'position')
    .modifyEager('cards', qb => {
      qb.select(
        'id', 'title', 'createdAt', 'positionY', 'positionX'
      );
      qb.where({ isArchived: false });
      qb.orderBy(['position_y', 'position_x']);
    })
    .where('deskId', deskId)
    .andWhere('is_archived', false)
    .orderBy('position')
};

exports.createList = async (deskId, listDTO) => {
  const { title } = listDTO;

  const { count } = await KanbanList.query()
    .count('id', {as: 'count'})
    .where({ deskId, isArchived: false })
    .first();

  return KanbanList.query().insert({
    deskId, title,
    position: count,
    width: constants.initialKanbanListWidth
  });
};

exports.updateListData = (listId, listDTO) => {
  const { title } = listDTO;

  return KanbanList.query()
    .update({ title })
    .where('id', listId)
    .first()
    .returning('*');
};

exports.updateListWidth = (deskList, width) => {
  return knex.transaction( async (trx) => {
    if (deskList.width <= width) {
      await KanbanList.query(trx)
        .patch({ width })
        .where({ id: deskList.id });

      return;
    };

    const cards = await KanbanCard.query(trx)
      .select('id', 'position_y', 'position_x')
      .where({ listId: deskList.id, isArchived: false })
      .orderBy(['position_y', 'position_x'])

    if (!!cards.length) {
      const updateCards = utils.changeListWidth(cards, width)
      await KanbanList.query(trx).upsertGraph(
        { id: deskList.id, width, kanbanCards: updateCards },
        { relate: true, unrelate: true }
      )
    } else {
      await KanbanList.query()
        .patch({ width })
        .where({ id: deskList.id });
    }

    return;
  });
};

exports.updateListPosition = (deskId, list, position) => {
  return knex.transaction( async (trx) => {
    let listArray = await KanbanList.query(trx)
      .where({ deskId, isArchived: false })
      .whereNot('id', list.id)
      .orderBy('position');

    let newPostiton = position;
    if (list.position <= position) {
      newPostiton -= 1;
    };


    listArray.splice(newPostiton, 0, list);
    listArray = listArray.map((item, index) => ({
      id: item.id, position: index
    }));

    await KanbanDesk.query(trx).upsertGraph(
      { id: deskId, kanbanLists: listArray },
      { relate: true, unrelate: true }
    );

    return;
  });
};

exports.deleteList = (deskId, listId) => {
  return knex.transaction( async (trx) => {
    const listArray = await KanbanList.query(trx)
      .where({ deskId, isArchived: false })
      .whereNot('id', listId)
      .orderBy('position')
      .then((value) => {
        return value.map((item, index) => ({
          id: item.id, position: index
        }))
      })


    const removeReq = KanbanList.query(trx).where('id', listId).del();
    const updatePositionsReq = KanbanDesk.query(trx).upsertGraph(
      { id: deskId, kanbanLists: listArray },
      { relate: true, unrelate: true }
    );

    return Promise.all([removeReq, updatePositionsReq]);
  })
};


exports.getCard = (cardId) => {
  return KanbanCard.query()
    .eager('author')
    .modifyEager('author', qb => qb.select('id', 'username'))
    .findOne({
      id: cardId,
      isArchived: false,
    });
};

exports.createCard = (user, list, cardDTO) => {
  const { title, position } = cardDTO;
  const { x: positionX, y: positionY } = position;

  return knex.transaction( async (trx) => {
    const listCards = await KanbanCard.query(trx)
      .select('id', 'position_y', 'position_x')
      .where({ listId: list.id, isArchived: false })
      .orderBy(['position_y', 'position_x']);

    const newCardList = utils.insertCardToList(listCards, list.width, {
      title, positionX, positionY,
      listId: list.id,
      deskId: list.deskId,
      ownerId: user.id
    });

    const kanbanList = await KanbanList.query(trx).upsertGraph(
      { id: list.id, kanbanCards: newCardList },
      { relate: true, unrelate: true }
    );

    const insertCard = kanbanList.kanbanCards.find(item =>
      item.positionX == positionX && item.positionY == positionY
    );

    return {
      id: insertCard.id,
      title: insertCard.title,
      author: { id: user.id, username: user.username },
      createdAt: insertCard.createdAt
    };
  });
};


exports.updateCardData = async (card, cardDTO) => {
  const updateData = {
    title: cardDTO.title,
    description: cardDTO.description
  };

  if (typeof updateData.title === 'undefined') {
    updateData.title = card.title
  }

  if (typeof updateData.description === 'undefined') {
    updateData.description = card.description
  }

  await KanbanCard.query()
    .update(updateData)
    .where({ id: card.id });

  return {
    id: card.id,
    title: updateData.title,
    author: card.author,
    description: updateData.description,
    createdAt: card.createdAt
  };
}

exports.updateCardPosition = (card, list, position) => {
  const { x: positionX, y: positionY } = position;

  return knex.transaction( async (trx) => {
    const cards = await KanbanCard.query(trx)
      .select('id', 'position_y', 'position_x')
      .where({ isArchived: false, listId: list.id })
      .orderBy(['position_y', 'position_x']);

    const newCardList = utils.insertCardToList(cards, list.width, {
      id: card.id, positionX, positionY
    });

    await KanbanList.query(trx).upsertGraph(
      { id: list.id, kanbanCards: newCardList },
      { relate: true }
    );

    return;
  });
};

exports.removeCard = (cardId) => {
  return KanbanCard.query()
    .where({ id: cardId })
    .del();
}
