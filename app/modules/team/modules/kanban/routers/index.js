const express = require('express');

const { hasPermission } = require('../../../middlewares');


const controllers = require('../controllers');
const validations = require('../validations');
const middlewares = require('../middlewares');

const kanbanListRoutes = require('./list')
const kanbanCardRoutes = require('./card')

const router = express.Router();

router.get('/', controllers.getKanbanDeskListByTeam);

router.post(
  '/',
  hasPermission('createDesk'),
  validations.kanbanDeskData,
  controllers.createKanbanDesk
);

router.get(
  '/:deskId',
  middlewares.checkKanbanDesk,
  controllers.getKanbanDeskWithList
);
router.put(
  '/:deskId',
  hasPermission('editDesk'),
  middlewares.checkKanbanDesk,
  validations.kanbanDeskData,
  controllers.updateKanbanDesk
);
router.delete(
  '/:deskId',
  hasPermission('removeDesk'),
  middlewares.checkKanbanDesk,
  controllers.removeKanbanDesk
);


router.use('/:deskId/lists', middlewares.checkKanbanDesk, kanbanListRoutes);
router.use('/:deskId/cards', middlewares.checkKanbanDesk, kanbanCardRoutes);

module.exports = router;
