const express = require('express');

const { hasPermission } = require('../../../middlewares');

const controllers = require('../controllers');
const validations = require('../validations');
const middlewares = require('../middlewares');

const router = express.Router();

router.get(
  '/:cardId',
  middlewares.checkKanbanCard,
  controllers.getKanbanCard
);

router.put(
  '/:cardId',
  hasPermission('editCard'),
  middlewares.checkKanbanCard,
  validations.updateCardData,
  controllers.updateKanbanCardData
);

router.put(
  '/:cardId/position/:listId',
  hasPermission('editCard'),
  middlewares.checkKanbanCard,
  middlewares.checkKanbanList,
  validations.updateCardPosition,
  controllers.updateKanbanCardPosition,
);

router.delete(
  '/:cardId',
  hasPermission('removeCard'),
  middlewares.checkKanbanCard,
  controllers.removeKanbanCard
);

module.exports = router;
