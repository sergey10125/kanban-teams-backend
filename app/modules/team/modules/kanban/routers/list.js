const express = require('express');

const { hasPermission } = require('../../../middlewares');

const controllers = require('../controllers');
const validations = require('../validations');
const middlewares = require('../middlewares');

const router = express.Router();

router.post(
  '/',
  validations.listData,
  controllers.createKanbanList
);

router.put(
  '/:listId',
  hasPermission('createList'),
  middlewares.checkKanbanList,
  validations.listData,
  controllers.updateKanbanListData
);
router.put(
  '/:listId/width',
  hasPermission('editList'),
  middlewares.checkKanbanList,
  validations.updateKanbanListWidth,
  controllers.updateKanbanListWidth
);
router.put(
  '/:listId/position',
  hasPermission('editList'),
  middlewares.checkKanbanList,
  validations.updateListPosition,
  controllers.updateKanbanListPosition
);

router.delete(
  '/:listId',
  hasPermission('removeList'),
  middlewares.checkKanbanList,
  controllers.removeKanbanList
);

router.post(
  '/:listId/card',
  hasPermission('createCard'),
  middlewares.checkKanbanList,
  validations.createCardData,
  controllers.createKanbanCard
);

module.exports = router;
