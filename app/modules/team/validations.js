const { check, body } = require('express-validator');
const checkValidation = require('@app/utils/checkValidation');

exports.invite = [
  check('token').exists(),
  checkValidation
];

exports.teamData = [
  body('title').exists().isString().isLength({ max: 255 }).not().isEmpty(),
  checkValidation
];

exports.inviteUser = [
  body('email').exists().isEmail(),
  checkValidation
];
