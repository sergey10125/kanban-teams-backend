const teamServiceId = 1;
const teamOwnerRoleId = 11;

const mainTeamChatTemplate = {
  title: "Global",
  isMain: true
};

module.exports = {
  teamServiceId,
  teamOwnerRoleId,
  mainTeamChatTemplate
}
