  const AppError = require('@app/utils/AppError');

const { teamService, mailService } = require('./services');
const { convertServicRoleArray } = require('./utils');

const TEAM_OWNER_ROLE_ID = 11;


exports.checkUserInviteToken = async (req, res, next) => {
  const { user, query, body } = req;
  const token = body.token || query.token;

  const tokenRecord = await teamService.getTokenData(token);

  if (!tokenRecord || tokenRecord.email !== user.email) {
    return next(new AppError(404));
  };

  req.tokenRecord = tokenRecord;
  return next();
}

exports.checkTeam = async (req, res, next) => {
  const { user, params: { teamId } } = req;

  const team = await teamService.getTeamWithUserPermission(teamId, user.id);
  if (!team) return next(new AppError(404));

  req.team = team;
  return next();
}

exports.hasPermission = (permission, isRequer = true) => (req, res, next) => {
  const { userRoles: { permissions } } = req.team;
  if (permissions.includes(permission) !== isRequer) {
    return next(new AppError(403));
  };

  return next();
}

exports.checkUnexistTeamMember = async (req, res, next) => {
  const { team, body: { email } } = req;

  const detectInTeam = await teamService.checkEmailInTeam(team.id, email);

  if (detectInTeam) {
    return next(new AppError(400, { email: 'alreadyExists' }));
  };

  return next();
}

exports.checkTeamInviteToken = async (req, res, next) => {
  const { team, query, body } = req;
  const token = body.token || query.token;

  const tokenRecord = await teamService.getTokenData(token);
  if (!tokenRecord || tokenRecord.teamId !== team.id) {
    return next(new AppError(404));
  };

  tokenRecord.roleArray = convertServicRoleArray(tokenRecord.roleArray);
  delete tokenRecord.teamId;

  req.teamTokenRecord = tokenRecord;
  return next();
}

exports.checkTeamMember = async (req, res, next) => {
  const { team, params: { userId } } = req;

  const member = await teamService.getTeamMember(team.id, userId);
  if (!member) return next(new AppError(404));

  member.roleArray = convertServicRoleArray(member.roleArray);
  req.member = member;
  return next();
}

exports.checkRoleArray = async (req, res, next) => {
  const { roleArray } = req.body;

  if (!roleArray || roleArray.some(id => id == TEAM_OWNER_ROLE_ID)) {
    return next(new AppError(400, { roleArray: 'Invalid role array' }));
  };

  const serviceRoles = await teamService.getServiceRoles(roleArray);

  if (roleArray.length !== serviceRoles.length) {
    return next(new AppError(400, { roleArray: 'Invalid role array' }));
  };

  req.serviceRoles = serviceRoles;
  return next();
}
