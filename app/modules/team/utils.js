exports.convertServicRoleArray = (serviceRoles) => {
  return serviceRoles.reduce((obj, value) => {
    const { serviceTitle, roleId, roleTitle } = value;
    obj[serviceTitle] = { id: roleId, title: roleTitle };
    return obj;
  }, new Object());
};
