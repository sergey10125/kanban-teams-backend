const socketManager = require('@app/socketManager');
const AppError = require('@app/utils/AppError');

const { teamService, mailService } = require('./services');
const { convertServicRoleArray } = require('./utils');

const { notification: socketNotification } = socketManager;


exports.getServiceWithRoles = async (req, res) => {
  const services = await teamService.getServiceWithRoles();
  return res.status(200).send(services)
}

exports.confirmInvite = async (req, res, next) => {
  const { user, tokenRecord } = req;

  const [ member, team ] = await Promise.all([
    teamService.confirmInvite(user, tokenRecord),
    teamService.getTeam(tokenRecord.teamId)
  ]);

  socketNotification.addTeamMember(team.id, tokenRecord.token, member);
  return res.status(200).send({
    id: team.id,
    title: team.title
  });
}


// standart CRUD team controllers

exports.getUserTeams = async (req, res) => {
  const { user } = req;
  const teamList = await teamService.getUserTeams(user.id);
  return res.status(200).send(teamList)
}

exports.getTeamWithPermission = (req, res, next) => {
  const { team } = req;
  const invite = socketManager.createInvite('team', { id: team.id });
  return res.status(200).send({ invite, team });
}

exports.createTeam = async (req, res, next) => {
  const { user, body: teamDTO } = req;
  const team = await teamService.createTeam(user.id, teamDTO);
  return res.status(200).send(team);
}

exports.updateTeam = async (req, res, next) => {
  const socketId = req.header('X-Socket-Id');
  const { team, body: teamDTO } = req;

  const [ updateTeam, members ] = await Promise.all([
    teamService.updateTeamData(team.id, teamDTO),
    teamService.getTeamMemberList(team.id)
  ]);

  const memberIds = members.map(item => item.id);

  socketNotification.editTeamData(socketId, updateTeam, memberIds);
  return res.status(200).send(updateTeam);
}

exports.leaveTeam = async (req, res, next) => {
  const socketId = req.header('X-Socket-Id');
  const { user, team } = req;

  await teamService.removeMember(team.id, user.id);

  socketNotification.removeTeamMember(socketId, team.id, user.id);
  return res.sendStatus(200);
}

exports.archivedTeam = async (req, res, next) => {
  const socketId = req.header('X-Socket-Id');
  const { user, team } = req;

  const members = await teamService.getTeamMemberList(team.id);
  const memberIds = members.map(item => item.id);

  await teamService.archivedTeam(team.id);

  socketNotification.deleteTeam(socketId, team.id, memberIds);
  return res.sendStatus(200);
}


// team invite token controllers

exports.getTeamInviteList = async (req, res, next) => {
  const { team } = req;
  const inviteList = await teamService.getTeamInviteList(team.id);
  return res.status(200).send(inviteList);
}

exports.getTeamInvite = async (req, res, next) => {
  const { teamTokenRecord } = req;
  return res.status(200).send(teamTokenRecord);
}

exports.inviteUser = async (req, res, next) => {
  const socketId = req.header('X-Socket-Id');
  const { team, serviceRoles, body: { email } } = req;

  const roleArray = serviceRoles.map(item => item.roleId);
  const tokenRecord = await teamService.createTokenRecord(
    team.id, email, roleArray
  );

  const responseData = {
    email: tokenRecord.email,
    token: tokenRecord.token
  };

  mailService.sendTeamInvite(email, tokenRecord.token);
  socketNotification.createTeamInvite(socketId, team.id, responseData);
  return res.status(200).send(responseData);
}

exports.updateInviteRoles = async (req, res, next) => {
  const socketId = req.header('X-Socket-Id');
  const { team, teamTokenRecord, serviceRoles} = req;

  const roleArray = serviceRoles.map(item => item.roleId);
  await teamService.updateInviteRoles(teamTokenRecord.token, roleArray);

  teamTokenRecord.roleArray = convertServicRoleArray(serviceRoles);

  socketNotification.updateTeamInvite(socketId, team.id, teamTokenRecord);
  return res.status(200).send(teamTokenRecord);
}

exports.deleteInvite = async (req, res, next) => {
  const socketId = req.header('X-Socket-Id');
  const { team, teamTokenRecord: { token } } = req;
  await teamService.deleteInviteToken(token);

  socketNotification.deleteTeamInvite(socketId, team.id, token);
  return res.sendStatus(200);
}

// team members controllers

exports.getTeamMemberList = async (req, res, next) => {
  const { team } = req;
  const memberList = await teamService.getTeamMemberList(team.id);
  res.status(200).send(memberList);
}

exports.getTeamMember = (req, res, next) => {
  const { member } = req;
  res.status(200).send(member);
}

exports.updateMemberRoles = async (req, res, next) => {
  const socketId = req.header('X-Socket-Id');
  const { team, member, serviceRoles } = req;

  await teamService.updateMemberRole(
    team.id, member.id, member.createAt, serviceRoles
  );

  const roleIdArr = serviceRoles.map(value => value.roleId);
  const memberNewRoles = await teamService.getPermissionsForRoles(roleIdArr);
  socketNotification.updateMemberRolesInTeam(
    socketId, team.id, member.id, memberNewRoles
  );

  member.roleArray = convertServicRoleArray(serviceRoles);
  socketNotification.updateTeamMember(socketId, team.id, member);
  return res.status(200).send(member);
}

exports.removeMember = async (req, res, next) => {
  const socketId = req.header('X-Socket-Id');
  const { team, member } = req;

  await teamService.removeMember(team.id, member.id);

  socketNotification.removeTeamMember(socketId, team.id, member.id);
  return res.sendStatus(200);
}
