const express = require('express');

const controllers = require('./controllers');
const middlewares = require('./middlewares');
const validations = require('./validations');

const childModules = require('./modules');

const router = express.Router();

router.get('/services/roles', controllers.getServiceWithRoles);


router.get('/', controllers.getUserTeams);
router.post('/', validations.teamData, controllers.createTeam);

router.get(
  '/invite/verification',
  validations.invite,
  middlewares.checkUserInviteToken,
  (req, res) => res.sendStatus(200)
);
router.post(
  '/invite/accept',
  validations.invite,
  middlewares.checkUserInviteToken,
  controllers.confirmInvite
);

router.get(
  '/:teamId',
  middlewares.checkTeam,
  controllers.getTeamWithPermission
);

router.put(
  '/:teamId',
  middlewares.checkTeam,
  middlewares.hasPermission('renameTeam'),
  validations.teamData,
  controllers.updateTeam
);
router.delete(
  '/:teamId/leave',
  middlewares.checkTeam,
  middlewares.hasPermission('ownerPermissions', false),
  controllers.leaveTeam
);
router.delete(
  '/:teamId',
  middlewares.checkTeam,
  middlewares.hasPermission('removeTeam'),
  controllers.archivedTeam
);



router.get(
  '/:teamId/members/invite',
  middlewares.checkTeam,
  middlewares.hasPermission('memberMenegment'),
  controllers.getTeamInviteList
);
router.get(
  '/:teamId/members/invite/fullinfo',
  middlewares.checkTeam,
  middlewares.hasPermission('memberMenegment'),
  middlewares.checkTeamInviteToken,
  controllers.getTeamInvite
);
router.post(
  '/:teamId/members/invite',
  middlewares.checkTeam,
  middlewares.hasPermission('memberMenegment'),
  validations.inviteUser,
  middlewares.checkUnexistTeamMember,
  middlewares.checkRoleArray,
  controllers.inviteUser
);
router.put(
  '/:teamId/members/invite',
  middlewares.checkTeam,
  middlewares.hasPermission('memberMenegment'),
  validations.invite,
  middlewares.checkTeamInviteToken,
  middlewares.checkRoleArray,
  controllers.updateInviteRoles
);
router.delete(
  '/:teamId/members/invite',
  middlewares.checkTeam,
  middlewares.hasPermission('memberMenegment'),
  validations.invite,
  middlewares.checkTeamInviteToken,
  controllers.deleteInvite
);

router.get(
  '/:teamId/members/',
  middlewares.checkTeam,
  controllers.getTeamMemberList
);
router.get(
  '/:teamId/members/:userId',
  middlewares.checkTeam,
  middlewares.checkTeamMember,
  controllers.getTeamMember
);
router.put(
  '/:teamId/members/:userId',
  middlewares.checkTeam,
  middlewares.hasPermission('memberMenegment'),
  middlewares.checkTeamMember,
  middlewares.checkRoleArray,
  controllers.updateMemberRoles
);
router.delete(
  '/:teamId/members/:userId',
  middlewares.checkTeam,
  middlewares.hasPermission('memberMenegment'),
  middlewares.checkTeamMember,
  controllers.removeMember
);

router.use(
  '/:teamId/chat/',
  middlewares.checkTeam,
  childModules.chatModule.routers
);

router.use(
  '/:teamId/kanban/',
  middlewares.checkTeam,
  childModules.kanbanModule.routers
);

module.exports = router;
