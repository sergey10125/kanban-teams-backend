const { transaction } = require('objection');
const crypto = require('crypto');

const {
  Team,
  TeamInvite,
  UserTeam,
  TeamRole,
  Service
} = require('@app/db/models');
const { knex } = require('@app/db');
const AppError = require('@app/utils/AppError');

const constants = require('../constants');


exports.getServiceWithRoles = () => {
  return Service.query()
    .eager('roles.[permissions]')
    .modifyEager('roles', builder =>
      builder.select('id', 'title').whereNot({
        id: constants.teamOwnerRoleId
      })
    )
}

exports.getServiceRoles = (roleArray) => {
  return TeamRole.query()
    .select('id', 'title')
    .eager('service')
    .whereIn('role.id', roleArray)
    .then((values) => {
      return values.map(value => ({
        roleId: value.id,
        roleTitle: value.title,
        serviceId: value.service.id,
        serviceTitle: value.service.title
      }))
    })
}

exports.getTokenData = async (token) => {
  const tokenRecord = await TeamInvite.query()
    .where({ token })
    .first()

  if (!tokenRecord) return null;

  tokenRecord.roleArray = await TeamRole.query()
    .select('id', 'title')
    .eager('service')
    .whereIn('id', tokenRecord.roleArray)
    .then((values) => {
      return values.map(value => ({
        roleId: value.id,
        roleTitle: value.title,
        serviceId: value.service.id,
        serviceTitle: value.service.title
      }))
    })

    return tokenRecord;
}


exports.checkEmailInTeam = (teamId, email) => {
  const checkInTeamMember = UserTeam.query()
    .joinRelation('user')
    .where('user.email', email)
    .andWhere('teamId', teamId)
    .first()
    .then((value) => !!value);


  const checkInTeamInvites = TeamInvite.query()
    .where('email', email)
    .andWhere('teamId', teamId)
    .first()
    .then((value) => !!value);

  return Promise.all([ checkInTeamMember, checkInTeamInvites ])
    .then(values => {
      const [ detectInMember, detectInInvite ] = values;
      return detectInMember || detectInInvite
    })
}

exports.checkEmailInTeamInvites = (teamId, email) => {
  return TeamInvite.query()
    .where('email', email)
    .andWhere('teamId', teamId)
    .first()
    .then((value) => !!value);
}

exports.getTeamMember = async (teamId, userId) => {
  const userTeamRecords = await UserTeam.query()
    .eager('user')
    .where({ teamId, userId });

  if (!userTeamRecords) return null;

  const roleIdArray = userTeamRecords.map(item => item.roleId);
  const user = userTeamRecords[0].user;

  const roleArray = await TeamRole.query()
    .select('id', 'title')
    .eager('service')
    .whereIn('id', roleIdArray)
    .then((values) => {
      return values.map(value => ({
        roleId: value.id,
        roleTitle: value.title,
        serviceId: value.service.id,
        serviceTitle: value.service.title
      }))
    })

  return {
    id: user.id,
    username: user.username,
    email: user.email,
    roleArray
  }
}

exports.confirmInvite = (user, tokenRecord) => {
  const { token, roleArray, teamId } = tokenRecord;
  const userId = user.id;

  return knex.transaction( async (trx) => {
    const removeInviteReq = TeamInvite.query(trx).where({ token }).del();
    const promiseArr = roleArray.map(({ roleId, serviceId }) => {
      return UserTeam.query(trx)
        .insert({ userId, teamId, serviceId, roleId });
    });

    const [ bindingRecord ] = await Promise.all([
      ...promiseArr, removeInviteReq
    ]);

    const teamRole = roleArray.find(item => item.serviceId === 1);

    return {
      id: user.id,
      username: user.username,
      email: user.email,
      createdAt: bindingRecord.createdAt,
      roleTitle: teamRole.roleTitle,
    }
  })
}


exports.getUserTeams = (userId) => {
  return Team.query()
    .select('team.id', 'team.title', 'team.createdAt')
    .joinRelation('users')
    .where('users.id', userId)
    .andWhere('team.isArchived', false)
    .andWhere('serviceId', constants.teamServiceId)
    .orderBy('createdAt', 'desc')
}

exports.getTeam = (teamId) => {
  return Team.query()
    .select('id', 'title', 'description', 'created_at')
    .findOne({ id: teamId })
}


const pareceMainRoleWithAllPermission = (roles) => {
  const teamServiceRole = { ...roles[0] };
  const permissionsMap = {};

  roles.forEach(role => {
    role.permissions.forEach(({ title: key }) => {
      permissionsMap[key] = true;
    });
  });

  teamServiceRole.permissions = Object.keys(permissionsMap);
  return teamServiceRole;
}

exports.getTeamWithUserPermission = (teamId, userId) => {
  return Team.query()
    .select('id', 'title', 'description', 'created_at')
    .findOne({ id: teamId, isArchived: false })
    .eager('userRoles.permissions')
    .modifyEager('userRoles', qb => {
      qb.select('role.title')
      qb.where('user_id', userId)
      qb.innerJoin('service', 'role.serviceId', 'service.id')
      qb.orderBy('role.serviceId')
    })
    .modifyEager('userRoles.permissions', qb => qb.select('title'))
    .then((value) => {
      if (!value || !value.userRoles.length) return null;
      value.userRoles = pareceMainRoleWithAllPermission(value.userRoles);
      return value;
    })
}

exports.getPermissionsForRoles = (roles) => {
  return TeamRole.query()
    .select('title')
    .whereIn('id', roles)
    .orderBy('serviceId')
    .eager('permissions')
    .modifyEager('permissions', qb => qb.select('title'))
    .then((values) => {
      if (!values.length) return null;
      return pareceMainRoleWithAllPermission(values);
    })
}

exports.createTeam = (userId, data) => {
  const { title, description } = data;

  return knex.transaction((trx) => {
    return Team.query(trx).insertGraph(
      {
        title, description,
        users: [{
          id: userId,
          service_id: constants.teamServiceId,
          role_id: constants.teamOwnerRoleId
        }],
        chatRooms: [ constants.mainTeamChatTemplate ]
      },
      { relate: true }
    ).then((team) => {
      delete team.chatRooms;
      delete team.users;
      return team;
    });
  })
}

exports.updateTeamData = (teamId, data) => {
  const { title, description } = data;
  return Team.query()
    .where('id', teamId)
    .patch({ title, description })
    .returning(['id', 'title', 'description', 'createdAt'])
    .first();
}

exports.archivedTeam = (teamId) => {
  return Team.query()
    .where('id', teamId)
    .update({ isArchived: true });
}


exports.getTeamInviteList = (teamId) => {
  return TeamInvite.query()
    .select('email', 'token')
    .where({ teamId });
}

exports.createTokenRecord = (teamId, email, roleArray) => {
  const token = crypto.randomBytes(20).toString('hex');
  return TeamInvite.query()
    .insert({ roleArray, teamId, email, token })
    .then((value) => {
      delete value.roleArray;
      delete value.teamId;
      return value
    })
}

exports.updateInviteRoles = (token, roleArray) => {
  return TeamInvite.query()
    .where({ token })
    .update({ roleArray });
}

exports.deleteInviteToken = (token) => {
  return TeamInvite.query()
    .where({ token })
    .del();
}


exports.getTeamMemberList = (teamId) => {
  return UserTeam.query()
    .select(
      'user.id',
      'user.username',
      'user.email',
      'UserTeam.created_at',
      'role.title as roleTitle'
    )
    .joinRelation('user')
    .joinRelation('role')
    .where('teamId', teamId)
    .andWhere('UserTeam.serviceId', constants.teamServiceId)
    .andWhere('user.isArchived', false)
}

exports.updateMemberRole = (teamId, userId, createdAt, roleArray) => {
  return knex.transaction((trx) => {
    const removeReq = UserTeam.query(trx)
      .delete().where({ userId, teamId });

    const insertReqArr = roleArray.map(({ roleId, serviceId }) =>
      UserTeam.query(trx).insert({
        teamId, serviceId, roleId, userId, createdAt
      })
    );

    return Promise.all([removeReq, ...insertReqArr]);
  })
}

exports.removeMember = (teamId, userId) => {
  return UserTeam.query().where({ userId, teamId }).del();
}
