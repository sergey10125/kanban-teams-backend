const jade = require('jade');
const sendEmail = require('@app/utils/sendEmail');

const template = require('./template');

exports.sendTeamInvite = (email, token) => {
  const context = template.teamInvite(token);
  return sendEmail(email, 'You are invited to team', context);
}
