const path = require('path');
const jade = require('jade');

const { app: { host }} = require('@config');

const templatePath = {
  teamInvite: path.join(__dirname, 'teamInvite.jade')
}

exports.teamInvite = (token) => {
  return jade.renderFile(templatePath.teamInvite, { host, token });
}
