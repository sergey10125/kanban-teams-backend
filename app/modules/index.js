const express = require('express');

const errorHandler = require('@app/utils/errorHandler')

const authModules = require('./auth');
const teamModules = require('./team');

const { checkAuthStatus } = authModules.middlewares;


const router = express.Router();

router.use('/auth', authModules.routers);
router.use('/team', checkAuthStatus(true), teamModules.routers);

router.use(errorHandler);

module.exports = router;
