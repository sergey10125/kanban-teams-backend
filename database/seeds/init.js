const actionArray = [
  // team actions
  {
    id: 1,
    service_id: 1,
    title: 'ownerPermissions'
  },
  {
    id: 2,
    service_id: 1,
    title: 'renameTeam'
  },
  {
    id: 3,
    service_id: 1,
    title: 'removeTeam'
  },
  {
    id: 4,
    service_id: 1,
    title: 'memberMenegment'
  },
  // team actions
  // kanban actions
  {
    id: 5,
    service_id: 2,
    title: 'createDesk'
  },
  {
    id: 6,
    service_id: 2,
    title: 'editDesk'
  },
  {
    id: 7,
    service_id: 2,
    title: 'removeDesk'
  },

  {
    id: 8,
    service_id: 2,
    title: 'createList'
  },
  {
    id: 9,
    service_id: 2,
    title: 'editList'
  },
  {
    id: 10,
    service_id: 2,
    title: 'removeList'
  },
  {
    id: 11,
    service_id: 2,
    title: 'createCard'
  },
  {
    id: 12,
    service_id: 2,
    title: 'editCard'
  },
  {
    id: 13,
    service_id: 2,
    title: 'removeCard'
  },
  // kanban actions
  // chat actions
  {
    id: 14,
    service_id: 3,
    title: 'createChatRoom'
  },
  {
    id: 15,
    service_id: 3,
    title: 'updateChatRoom'
  },
  {
    id: 16,
    service_id: 3,
    title: 'removeChatRoom'
  },
  {
    id: 17,
    service_id: 3,
    title: 'createChatMessage'
  },
  // chat actions
]

exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('service').del()
    .then(() => knex('service').insert([
      {id: 1, title: 'team'},
      {id: 2, title: 'kanban'},
      {id: 3, title: 'chat'},
    ]))
    .then(() => knex('action').del())
    .then(() => knex('action').insert(actionArray))
    .then(() => knex('role').del())
    .then(() => knex('role').insert([
      // team roles
      {
        id: 11,
        service_id: 1,
        title: 'owner',
        position: 3
      },
      {
        id: 12,
        service_id: 1,
        title: 'admin',
        position: 2
      },
      {
        id: 13,
        service_id: 1,
        title: 'user',
        position: 1
      },
      // team roles

      // kanban roles
      {
        id: 14,
        service_id: 2,
        title: 'admin',
        position: 4
      },
      {
        id: 15,
        service_id: 2,
        title: 'moderator',
        position: 3
      },
      {
        id: 16,
        service_id: 2,
        title: 'user',
        position: 2
      },
      {
        id: 17,
        service_id: 2,
        title: 'guest',
        position: 1
      },
      // kanban roles
      // chat roles
      {
        id: 18,
        service_id: 3,
        title: 'admin',
        position: 2,
      },
      {
        id: 19,
        service_id: 3,
        title: 'user',
        position: 1,
      },
    ]))
    .then(() => knex('action_permission').del())
    .then(() => {
      const actionsIdWithoutTeam = actionArray
        .filter(item => item.service_id !== 1)
        .map(item => item.id)

      return knex('action_permission').insert([].concat(
        { action_id: 1, role_id: 11 },
        { action_id: 2, role_id: 11 },
        { action_id: 3, role_id: 11 },
        { action_id: 4, role_id: 11 },
        actionsIdWithoutTeam.map(id => ({ action_id: id, role_id: 11 })),

        { action_id: 4, role_id: 12 },
        actionsIdWithoutTeam.map(id => ({ action_id: id, role_id: 12 })),

        { action_id: 5, role_id: 14 },
        { action_id: 6, role_id: 14 },
        { action_id: 7, role_id: 14 },
        { action_id: 8, role_id: 14 },
        { action_id: 9, role_id: 14 },
        { action_id: 10, role_id: 14 },
        { action_id: 11, role_id: 14 },
        { action_id: 12, role_id: 14 },
        { action_id: 13, role_id: 14 },
        { action_id: 14, role_id: 14 },

        { action_id: 8, role_id: 15 },
        { action_id: 9, role_id: 15 },
        { action_id: 10, role_id: 15 },
        { action_id: 11, role_id: 15 },
        { action_id: 12, role_id: 15 },
        { action_id: 13, role_id: 15 },
        { action_id: 14, role_id: 15 },

        { action_id: 11, role_id: 16 },
        { action_id: 12, role_id: 16 },
        { action_id: 13, role_id: 16 },

        { action_id: 14, role_id: 18 },
        { action_id: 15, role_id: 18 },
        { action_id: 16, role_id: 18 },
        { action_id: 17, role_id: 18 },

        { action_id: 17, role_id: 19 },
      )
    )})
};
