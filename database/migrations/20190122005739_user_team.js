
exports.up = function(knex, Promise) {
  return knex.schema.createTable('user_team', (table) => {
    table.integer('user_id').unsigned()
    table.integer('team_id').unsigned()
    table.integer('service_id').unsigned()
    table.integer('role_id').unsigned()
    table.timestamp('created_at').defaultTo(knex.fn.now())

    table.primary(['user_id', 'team_id', 'service_id']);

    table.foreign('user_id')
      .references('user.id')
      .onDelete('CASCADE')

    table.foreign('team_id')
      .references('team.id')
      .onDelete('CASCADE')

    table.foreign('service_id')
      .references('service.id')
      .onDelete('CASCADE')

    table.foreign('role_id')
      .references('role.id')
      .onDelete('CASCADE')
  })
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTableIfExists('user_team')
};
