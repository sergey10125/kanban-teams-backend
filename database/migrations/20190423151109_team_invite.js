
exports.up = function(knex, Promise) {
  return knex.schema.createTable('team_invite', (table) => {
    table.integer('team_id').unsigned().notNullable();
    table.string('email').notNullable();
    table.string('token', 255).notNullable();
    table.specificType('role_array', 'INT[]').notNullable();;

    table.unique(['email', 'team_id']);

    table.foreign('team_id')
      .references('team.id')
      .onDelete('CASCADE');
  })
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTableIfExists('team_invite')
};
