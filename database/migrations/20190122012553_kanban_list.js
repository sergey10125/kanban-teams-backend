
exports.up = function(knex, Promise) {
  return knex.schema.createTable('kanban_list', (table) => {
    table.increments()
    table.integer('desk_id').unsigned().notNullable()
    table.string('title', 255).notNullable()
    table.integer('position').unsigned().notNullable()
    table.integer('width').unsigned().defaultTo(2).notNullable()
    table.boolean('is_archived').defaultTo(false)

    table.foreign('desk_id')
      .references('kanban_desk.id')
      .onDelete('CASCADE')
  })
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTableIfExists('kanban_list')
};
