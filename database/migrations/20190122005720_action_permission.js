
exports.up = function(knex, Promise) {
  return knex.schema.createTable('action_permission', (table) => {
    table.integer('action_id').unsigned()
    table.integer('role_id').unsigned()

    table.unique(['action_id', 'role_id'])

    table.foreign('action_id')
      .references('action.id')
      .onDelete('CASCADE')

    table.foreign('role_id')
      .references('role.id')
      .onDelete('CASCADE')
  })
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTableIfExists('action_permission')
};
