
exports.up = function(knex, Promise) {
  return knex.schema.createTable('chat_room', (table) => {
    table.increments()
    table.integer('team_id').unsigned().notNullable();
    table.boolean('is_main').default(false);
    table.string('title').notNullable();
    table.datetime('created_at');

    table.foreign('team_id')
      .references('team.id')
      .onDelete('CASCADE')
  })
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTableIfExists('chat_room')
};
