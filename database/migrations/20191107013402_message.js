
exports.up = function(knex, Promise) {
  return knex.schema.createTable('chat_message', (table) => {
    table.increments()
    table.integer('room_id').unsigned().notNullable();
    table.integer('author_id').unsigned().notNullable();
    table.text('content').notNullable();
    table.datetime('created_at');

    table.foreign('room_id')
      .references('chat_room.id')
      .onDelete('CASCADE')

    table.foreign('author_id')
      .references('user.id')
      .onDelete('CASCADE')
  })
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTableIfExists('chat_message')
};
