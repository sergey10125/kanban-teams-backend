
exports.up = function(knex, Promise) {
  return knex.schema.createTable('action', (table) => {
    table.increments()
    table.integer('service_id').unsigned()
    table.string('title', 255).notNullable()

    table.unique(['service_id', 'title'])
    table.foreign('service_id')
      .references('service.id')
      .onDelete('CASCADE')
  })
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTableIfExists('action')
};
