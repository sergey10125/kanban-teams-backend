
exports.up = function(knex, Promise) {
  return knex.schema.createTable('kanban_desk', (table) => {
    table.increments()
    table.integer('team_id').unsigned()
    table.integer('owner_id').unsigned()
    table.string('title', 255).notNullable()
    table.boolean('is_archived').defaultTo(false)
    table.timestamp('created_at').defaultTo(knex.fn.now())

    table.foreign('team_id')
      .references('team.id')
      .onDelete('CASCADE')

    table.foreign('owner_id')
      .references('user.id')
      .onDelete('CASCADE')
  })
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTableIfExists('kanban_desk')
};
