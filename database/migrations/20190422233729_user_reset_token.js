
exports.up = function(knex, Promise) {
  return knex.schema.createTable('user_reset_password', (table) => {
    table.integer('user_id').unsigned().primary();
    table.string('reset_password_token', 255).nullable();
    table.timestamp('reset_password_expired').nullable();
  })
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTableIfExists('user_reset_password')
};
