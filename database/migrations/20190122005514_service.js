
exports.up = function(knex, Promise) {
  return knex.schema.createTable('service', (table) => {
    table.increments()
    table.string('title', 255).notNullable()
  })
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTableIfExists('service')
};
