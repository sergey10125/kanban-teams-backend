
exports.up = function(knex, Promise) {
  return knex.schema.createTable('team', (table) => {
    table.increments()
    table.string('title', 255).notNullable()
    table.text('description').defaultTo(null)
    table.boolean('is_archived').defaultTo(false)
    table.timestamp('created_at').defaultTo(knex.fn.now())
  })
}

exports.down = function(knex, Promise) {
  return knex.schema.dropTableIfExists('team')
};
