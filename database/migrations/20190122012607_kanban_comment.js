
exports.up = function(knex, Promise) {
  return knex.schema.createTable('kanban_comment', (table) => {
    table.increments()
    table.integer('card_id').unsigned()
    table.integer('owner_id').unsigned()
    table.text('message')
    table.timestamp('created_at').defaultTo(knex.fn.now())

    table.foreign('card_id')
      .references('kanban_card.id')
      .onDelete('CASCADE')

    table.foreign('owner_id')
      .references('user.id')
      .onDelete('CASCADE')
  })
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTableIfExists('kanban_comment')
};
