
exports.up = function(knex, Promise) {
  return knex.schema.createTable('message_notification', (table) => {
    table.integer('message_id').unsigned().notNullable();
    table.integer('user_id').unsigned().notNullable();

    table.foreign('message_id')
      .references('chat_message.id')
      .onDelete('CASCADE')

    table.foreign('user_id')
      .references('user.id')
      .onDelete('CASCADE')
  })
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTableIfExists('message_notification')
};
