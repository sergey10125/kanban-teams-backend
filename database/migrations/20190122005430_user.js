
exports.up = function(knex, Promise) {
  return knex.schema.createTable('user', (table) => {
    table.increments()
    table.string('username', 254).notNullable()
    table.string('email', 254).notNullable()
    table.string('phone', 30)
    table.string('password', 60).notNullable()
    table.boolean('is_archived').defaultTo(false)
    table.timestamp('updated_at').defaultTo(knex.fn.now())
    table.timestamp('created_at').defaultTo(knex.fn.now())

    table.unique('email')
  });
}

exports.down = function(knex, Promise) {
  return knex.schema.dropTableIfExists('user')
};
