
exports.up = function(knex, Promise) {
  return knex.schema.createTable('role', (table) => {
    table.increments()
    table.integer('service_id').unsigned()
    table.string('title', 255).notNullable()
    table.integer('position').unsigned()
    
    table.foreign('service_id')
      .references('service.id')
      .onDelete('CASCADE')
  })
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTableIfExists('role')
};
