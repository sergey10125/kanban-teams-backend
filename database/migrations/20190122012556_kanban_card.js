
exports.up = function(knex, Promise) {
  return knex.schema.createTable('kanban_card', (table) => {
    table.increments()
    table.integer('list_id').unsigned().notNullable()
    table.integer('desk_id').unsigned().notNullable()
    table.integer('owner_id').unsigned().notNullable()
    table.string('title', 255).notNullable()
    table.text('description')
    table.timestamp('created_at').defaultTo(knex.fn.now())
    table.integer('position_x').unsigned().notNullable()
    table.integer('position_y').unsigned().notNullable()
    table.boolean('is_archived').defaultTo(false).notNullable()

    table.foreign('list_id')
      .references('kanban_list.id')
      .onDelete('CASCADE')

    table.foreign('owner_id')
      .references('user.id')
      .onDelete('CASCADE')
  })
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTableIfExists('kanban_card')
};
