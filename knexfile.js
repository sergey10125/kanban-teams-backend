const path = require('path');
const { database: connection } = require('./config')

const additanalOption = {
  migrations: {
    tableName: 'knex_migrations',
    directory: path.resolve(__dirname, 'database/migrations'),
  },
  seeds: {
    directory:  path.resolve(__dirname, 'database/seeds'),
  }
}

module.exports = {
  development: {
    client: 'pg',
    connection: connection,
    ...additanalOption,
  },
  staging: {
    client: 'pg',
    connection: connection,
    ...additanalOption,
    pool: {
      min: 2,
      max: 10
    },
  },
  production: {
    client: 'pg',
    connection: connection,
    ...additanalOption,
    pool: {
      min: 2,
      max: 10
    }
  }
};
