require('module-alias/register');

const app = require('./app');
const { app: { port } } = require('./config');

app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});
